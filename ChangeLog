2025-01-31  Peter Baumgartner  <Peter.Baumgartner@data61.csiro.au>

	* Version 0.9.14
	* Updated to Scala compiler 2.12.19

2023-08-23  Peter Baumgartner  <Peter.Baumgartner@data61.csiro.au>

	* Version 0.9.13
	* Implemented SMT-Lib Version 2.6 "match" expressions.
	* Implemented SMT-Lib Version 2.6 indexed identifiers and "is" for datatype


2022-12-19  Peter Baumgartner  <Peter.Baumgartner@data61.csiro.au>

	* Version 0.9.11
	* Implemented SMT-Lib Version 2.6 datatypes; see README for more details

2017-03-02  Baumgartner  <Peter.Baumgartner@data61.csiro.au>

	* Version 0.9.9
	* Some efficiency improvements. Thanks, Josh!

2016-12-16  Baumgartner  <Peter.Baumgartner@data61.csiro.au>

	* Version 0.9.8
	* Bug fix: variable names with a ' now enabled
	* 'Better' translation of non-allowed chars in variables,
	  e.g. & becomes _ampersand_
	* 'formulas' in TPTP output are now numbered

2016-04-04  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.7
	* Added support for define-fun-rec and define-funs-rec (Josh)

2015-12-04  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.6
	* Faster parsing with parboiled2
	* Added Josh as a co-author

2015-11-24  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.5
	* Use BigInt instead of Int (Josh's pull request)

2015-10-13  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.4
	* Fully support term-level boolean sort
	* Flag --keepBool is obsolete

2015-09-21  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.3
	* Added a flag --keepBool for not eliminating Bool-sorted terms
	  (SMTtoTPTP is incomplete in this regard anyway)

2015-09-07  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Increased stack limit in smttotptp script to -Xss10M.
	This is needed to avoid a stack overflow error during parsing of
	deeply nested formulas

2015-08-31  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.2
	* Bug fix: some let-terms were left unexpanded
	* Bug fix: quantified boolean variables were not expanded.
	The fix consists in expansion of these variable by
	true/false for lack of a better idea
	* new flag: --pp: pretty-printing, now explicitly have to enable
	pretty-printing because it may significantly increase file size
	* Added obvious boolean simplification for removing true/false
	introduced by boolean quantifier elimination

2015-05-13  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9.1
	* Add support for TFF $distinct as a target predicate for
	  translating top-level "distinct" assertions into

2015-05-08  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.9
	- push and pop commands now lead to an error instead of being
	ignored. This is really neccessary to avoid mangling up
	the formula blocks into one big block
	- Real is now translated into $real (was: $rat)

2015-05-07  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>


2014-05-30  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.8
		- fixed a bug: generated tptp variable names were potentially not unique

2014-03-20  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.7
		- Fixed a bug in translating :prec options

2014-03-06  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.6
		- Honour set-info and set-option commands:
		  set-info commands go into the output as TPTP comments,
		  set-option commands as well, but using %$ as the
		  comment start. This enables passing on options to TPTP
	          provers
	        - Much improved handling of variables: any legal SMT-LIB
		  variable is handled now
	        - Improved error messages for parsing errors
	        - Miscellaneous minor fixes

2014-02-10  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.5
		- Include "SZS status Success" and "SZS output start ListOfTFF"/"SZS output end ListOfTFF"
		  lines in TPTP output

2014-02-07  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.4:
		- Pretty printing of generated tff formulas and diagnostic
		  output
		- Support define-const statement, as in
		  (define-const size Int 8)

2014-02-05  Peter Baumgartner  <Peter.Baumgartner@nicta.com.au>

	* Version 0.3:
		- Improved support for let-expressions
		- ite-expressions implemented
		- Some options have changed! See README for new options

