SMTtoTPTP - A translator from SMT-LIB to TPTP TFF
=================================================

SMTtoTPTP is a translator from problems written in the
[SMT-LIB](http://www.smtlib.org/) language, version 2.6, to the
[TPTP TFF](http://dx.doi.org/10.1007%2F978-3-642-28717-6_32)
language.

SMTtoTPTP supports (quantified) formulas over the combined theories of of free
symbols, arrays, integer and real arithmetic.  
Since version 0.9.11 it supports SMT-Lib version 2.6 datatype declarations,
and it also supports Z3-style datatype declarations.
Axioms for the declared datatypes and arrays are
automatically generated. 

SMTtoTPTP is written in [Scala](http://www.scala-lang.org). SMTtoTPTP is
provided in both source form and as a Java archive that can be executed by a
Java runtime system.

See [this paper](https://peba62.github.io/publications/SMTtoTPTP.pdf) for a
more detailled system description paper on SMTtoTPTP.

Installation and Running SMTtoTPTP
==================================

SMTtoTPTP is available at https://bitbucket.org/peba123/smttotptp/ .
The distribution includes the Scala source. 

Running the pre-compiled Java archive
-------------------------------------

Run

    java -jar /PATH/TO/HERE/target/scala-2.12/smttotptp-0.9.14.jar [OPTION...] INFILE [OUTFILE]

where INFILE is an SMT-LIB (version 2) file.

Applied to, e.g., `examples/datatype.smt2` this results in
`examples/datatype.p`

Run

    java -jar /PATH/TO/HERE/target/scala-2.12/smttotptp-0.9.14.jar --help

to obtain basic usage information.

Alternatively to `java -jar ....` run the shell script `smttotptp`.


Installation from sources
-------------------------

The SMTtoTPTP software can be built with sbt, the Scala Build Tool.
The sbt tool installs the scala compiler, if needed.

If SMTtoTPTP is to be compiled once-and-forall the probably easiest way 
is to create a Java jar-archive and execute that.
Steps:

(1) Install [sbt](http://www.scala-sbt.org/).

(2) In the current directory invoke `sbt assembly`. 

This creates the file `target/scala-2.12/smttotptp-0.9.14.jar`.

Caveat: the file `project/plugins.sbt` contains a line like 

    addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "1.1.1")

Please update the specified version string (here: "1.1.1") with
a more recent one from https://github.com/sbt/sbt-assembly
should this version not be available.

(3) Run

    java -jar target/scala-2.12/smttotptp-0.9.14.jar [OPTION...] INFILE [OUTFILE]

as described above.

Running out of Memory
---------------------

Occasionally it is helpful to enable higher ressource limits.
If you are encountering, e.g., heap problems, try adding the option

	-Xms512M -Xmx4G -Xss10M -XX:MaxPermSize=384M

to the java call.


Command Line Options
====================

	Usage: "java -jar as above" [OPTION ...] INFILE [OUTFILE]

Transform the SMT-Lib file `INFILE` into TPTP TFF format in `OUTFILE`.
If `OUTFILE` is not given its name is derived from `INFILE`.
`OPTION`:

	--help:      this message
	--version:   show the version number and exit
	--lists:     declare the list datatype and emit corresponding axioms
	--arrays:    declare store/select/const array operators and emit corresponding axioms
	--quiet:     suppress all diagnostic output to stdout
	--pp:        pretty-print the TPTP formula using indentation. (May lead to significantly larger file.)
    --stdout:    print result to stdout instead of OUTFILE. Implies --quiet

Limitations
===========

SMTtoTPTP is meant to support a comprehensive subset of the SMT-LIB language 
and the logics and theories in [1]. Yet, there are several limitations.

Naturally, many commands, such as `check-sat` are meaningless for SMTtoTPTP
and are simply ignored. Others, like `push` and `pop` lead to errors.
SMTtoTPTP has its own (small) set of options, see
below.  Term annotations are ignored as well.

SMTtoTPTP does not accept SMT-LIB theory files. Instead, the core theory, 
integer and real arithmetic and arrays (with extensionality) are built-in. 

Hexadecimals and binaries are not supported.

Support for SMT-Lib Version 2.6 should be fairly complete. Indexed identifiers as in `(_ a
5)` and testers for datatypes are supported, as is the the `match` statement
as of SMTtoTPTP version 0.9.13.

Extensions
==========

SMTtoTPTP features some extensions of the SMT-LIB language [1] 
for better compatibility with [Z3](http://z3.codeplex.com/), as follows.

declare-const/define-const
--------------------------

Constants can be declared as 

	(declare-const k Int)

instead of the slightly more verbose 

	(declare-fun () k Int)

Similarly, one can use

	(define-const k Int 5)

to declare and define a constant.


Z3-Style datatype declarations
------------------------------

Z3-Style datatype declarations are supported through the command
`declare-z3-datatype`. In version 0.9.10 and earlier this command was called
`declare-datatypes`. Since version 0.9.11 the command `declare-datatypes` (and `declare-datatype`)
is now reserved for SMT-Lib Verison 2.6 datatype declarations, see below.

For example, the usual list datatype can be declared as
 
	(declare-z3-datatype (T) ((List nil (insert (head T) (tail (List T))))))

which translates to the following declarations

	(declare-sort List 1)
	(declare-parametric-fun (T) nil () (List T)) ;; notice free sort parameter T
	(declare-parametric-fun (T) insert (T (List T)) (List T))
	(declare-parametric-fun (T) head ((List T)) T)
	(declare-parametric-fun (T) tail ((List T)) (List T))

For each sort instance used, these declarations are translated into corresponding TFF 
type declarations and axioms. For example, for the Int-instance these axioms are as follows, 
displayed with sort-stripped identifiers:

	tff(list, axiom, ( ! [ K: $int, L: list ] : head(insert(K, L)) = K )).
	tff(list, axiom, ( ! [ K: $int, L: list ] : tail(insert(K, L)) = L )).
	tff(list, axiom, ( ! [ K: $int, L: list ] : insert(K, L) != nil)).
	tff(list, axiom, ( ! [ L: list ] : ( L = nil | L = insert(head(L), tail(L)) ))).

See `examples/datatype.smt2` and the translated file `examples/datatype.p` for more examples.

Limitation: only one datatype can be declared per declare-z3-datatype command.

SMT-Lib 2.6 datatype declarations
---------------------------------
See the SMT-Lib 2.6 manual for details. Some examples from the manual:

	(declare-datatype Color ((red) (green) (blue)) )

	(declare-datatypes ( (List 1) ) (
       (par (T) ( (list-nil) (cons (car T) (cdr (List T)) )))))

	(declare-datatypes ( (Tree 1) (TreeList 1) ) (
      (par (X) ( (node (value X) (children (TreeList X)) ))) 
      (par (Y) ( (empty)
                 (insert (head (Tree Y)) (tail (TreeList Y))) ))))

Implementation is by conversion to Z3-style datatype declarations.

The `is` predicate is not yet implemented.

:named Attributes
=================

SMTtoTPTP honours certain `:named` attribute values. Values starting with
`axiom`, `hypothesis` or `conjecture` that are `:named`-attributed to top-level
`asserts` are taken as indications of corresponding TPTP formula kinds. 
Examples:

    (assert (! (= (+ 1 1) 2) :named hypothesis-1))
    (assert (! (= (+ 1 1) 2) :named axiom-1))
    (assert (! (= (+ 1 1) 2) :named conjecture-1))

are translated into, respectively,

    tff(hypothesis_1, hypothesis, ($sum(1, 1) = 2)).
    tff(axiom_1, axiom, ($sum(1, 1) = 2)).
    tff(conjecture_1, conjecture, ($sum(1, 1) != 2)).

Notice that `conjecture`s are implicitly negated!

Options
=======

Lists
-----

	(set-option :declare-list-datatype false)   (default)
	(set-option :declare-list-datatype true)    (command line option: --lists)

If true has the same effect as the command

   	(declare-z3-datatype (T) ((List nil (insert (head T) (tail (List T))))))

Arrays
------

	(set-option :declare-arrays false)         (default)
	(set-option :declare-arrays true)	   (command line option: --arrays)

If true, declarations are added as follows:

   	(declare-sort Array 2)
	(declare-parametric-fun (I E) select ((Array I E) I) E)
	(declare-parametric-fun (I E) store ((Array I E) I E) (Array I E))
	(declare-parametric-fun (I E) const (E) (Array I E))

Also axioms are generated for the select and store operators,
for all concrete sort instances of the array declarations (with extensionality)
as specified in [1].

The expression

	(const c)

means the array that has the element c everywhere.
It is defined as follows, for all concrete sorts I and E

	(forall ((i I) (e E)) (= (select ((as const (Array I E)) e) i) e))

See examples/array.smt2 and the translated file examples/array.p for more examples.

Let
---

	(set-option :expand-let true)       (default)
	(set-option :expand-let false)

If true expands let-terms by using existential quantifiers,
otherwise let-terms are translated into a *non-standard* TPTP let-form.

More precisely, this affects only expansion of non-boolean variables bound 
in a let-term. Boolean variables are always eliminated by in-place expansion.

Boolean variables bound by an existential or universal quantifier are always
eliminated by expansion into true/false and using "or" and "and",
respectively. Expect exponential blow-up.


distinct
--------

	(set-option :expand-distinct true)       (default)
	(set-option :expand-distinct false)

If true expands always expands assertions (distinct t_1 ... t_n)
into binary distinct-terms. If false, such top-level assertions are
mapped to a TFF fact with the $distinct predicate symbol.

If-then-else
------------

	(set-option :expand-ite true)       (default)
	(set-option :expand-ite false)

If true, expands ite-terms. 
Limitation: requires enabling of expansion of let-terms, if there are any,
cf the option :expand-let above.
This limitation will be removed in a future release.

Elimation of term-level Booleans
--------------------------------
The SMT-LIB language does not syntactically distinguish between terms and
formulas, everything is a term. Consequently, Boolean-sorted terms may occur
everywhere. In contrast, in the TPTP language Boolean typed terms may not
occur within term.

	(set-option :elim-tl-bool true)       (default)
	(set-option :elim-bool false)

If true, boolean-sorted terms within non-boolean sorted terms, i.e., those
terms that cannot directly be translated into TPTP, are transformed away (read
"tl" as "term-level"). The transformation introduces a quasi-boolean TPTP type
and extracts offending terms by introducing a new name and global definitions
for these names. The transformation is taken from the
[FOOL paper](http://link.springer.com/chapter/10.1007/978-3-319-20615-8_5#).

	(set-option :expand-bool-quant false)       (default)
	(set-option :expand-bool-quant true)

If true, expands boolean quantification by substituting true/false. For example

	(forall ((b Bool)) (ite b foo bar))

gets

	(and (ite true foo bar) (ite false foo bar))

which is simplified to

	(and foo bar)

Because expansion is not always useful it is disabled by default.

Other
-----

`set-option` commands are repeated as comments in the output TPTP file. 
The comment starts with %$ which can act as directive for TPTP software. 

For example, for specifying the operator precedences `f > g > h` one uses

	(set-option :prec "f > g > h")

which translates into the line

	%$ :prec f > g > h

The `:prec` option is indeed a special case; it accepts lists of strings, e.g.:

	(set-option :prec ("f > g > h" "f > k > l"))

which translate into the lines

	%$ :prec f > g > h
	%$ :prec f > k > l

Such declarations are honoured by the Beagle theorem prover.


Citation
========

Please use the following Bibtex entry:

	@inproceedings{Baumgartner:SMTtoTPTP:CADE:2015,
		author = {Peter Baumgartner},
		title = {{SMTtoTPTP} -- {A} {C}onverter for {T}heorem {P}roving {F}ormats},
		booktitle = {CADE-25 -- The 25th International Conference on Automated
		Deduction},
		editor = {Amy Felty and Aart Middeldorp},
		year = {2015},
		series = {LNAI},
		volume = {9195},
		pages = {285--294},
		series = {LNAI},
		publisher = {Springer}
	}

Authors
=======

[Peter Baumgartner](https://peba62.github.io/)

Former:
Josh Bax

Email: Peter.Baumgartner@data61.csiro.au

Usage reports, comments and suggestions are very welcome!

