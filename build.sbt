// import AssemblyKeys._

name := "smttotptp"

version := "0.9.14"

scalaVersion := "2.12.19"

scalaSource in Compile := baseDirectory.value / "src/"

resolvers ++= Seq(
  "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases",
//  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
  "Typesafe Repository" at "https://repo.typesafe.com/typesafe/maven-releases/"
)


// unmanagedBase in (Compile, run) <<= baseDirectory(_ / "lib/")

mainClass in (Compile, run) := Some("smttotptp.smttotptp")

retrieveManaged := true

//libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

libraryDependencies += "org.parboiled" %% "parboiled" % "2.1.8"

scalacOptions += "-g:vars"

// scalacOptions += "-unchecked"

// scalacOptions += "-optimise"

// Use Scala from a directory on the filesystem instead of retrieving from a repository
// scalaHome := Some(file("/usr/local/scala")),

scalacOptions ++= List("-feature", "-deprecation")

scalacOptions ++= List("-language:implicitConversions", "-language:postfixOps")

// assemblySettings

mainClass in assembly := Some("smttotptp.smttotptp")

assemblyJarName in assembly := "smttotptp-" + version.value + ".jar"

test in assembly := {}

