;; Z3 options:
(set-logic AUFLIA)
(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)

;; smttotptp options:
;; Not needed since Version 0.9.1 if set-logic includes arrays
;; (set-option :declare-arrays true)

;; abbreviation:
(define-sort IArray () (Array Int Int))

;; Max definition
(declare-fun maxA (IArray Int) Int)
(assert (forall ((a IArray) (n Int) (w Int))
		(=> (and (forall ((i Int))
				 (=> (and (> n i) (>= i 0)) 
				     (<= (select a i) w) ))
			 (exists ((i Int))
				 (and (> n i)
				      (>= i 0) 
				      (= (select a i) w))))
		    (= (maxA a n) w))))
  
;; Max test:
(declare-const a IArray)
(assert (= (select a 0) 0))
(assert (= (select a 1) 5))
(assert (= (select a 2) 3))
(assert (not (= (maxA a 2) 5)))
(check-sat)

;; ;; (const c) is the array that has the element c everywhere
;; ;; It is pre-declared, as follows:
;; ;; (declare-parametric-fun (I E) const (E) (Array I E))
;; ;; It is defined as follows, for all concrete sorts I and E
;; ;; (forall ((i I) (e E)) (= (select ((as const (Array I E)) e) i) e))

;; ;; const-test
;; (assert (not (> (select ((as const (Array Int Int)) 5) 2) 0)))
;; (check-sat)

