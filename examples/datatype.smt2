(set-logic AUFLIA)

(set-option :declare-list-datatype true)
;; equivalent to (declare-z3-datatype (T) ((List nil (insert (head T) (tail (List T))))))

;; Z3-style enumeration datatype
(declare-z3-datatype () ((Color red green blue)))

;; define-sort example using the above
(define-sort ColorList () (List Color))

;; record datatype
(declare-z3-datatype (S T) ((Pair (mk-pair (first S) (second T)))))

;; SMTLib 2.6 datatype declarations

(declare-datatype MyColor ((my-red) (my-green) (my-blue)) )

(declare-datatypes ( (MyList 1) ) (
   (par (T) ( (list-nil) (cons (car T) (cdr (MyList T)) )))))

(declare-datatypes ( (Tree 1) (TreeList 1) ) (
   (par (X) ( (node (value X) (children (TreeList X)) ))) 
   (par (Y) ( (empty)
              (tree-insert (tree-head (Tree Y)) (tree-tail (TreeList Y))) ))))


;; some random assertions:
(assert 
 (forall ((l (List Int))) (= (insert (head l) l) (tail l) (as nil (List Int)))))

(declare-const a (Array Int ColorList))

(assert (= (select a 2) (insert red (as nil ColorList))))

(assert (= (mk-pair 1 2) (mk-pair 3 4)))

(assert (let ((c green)) (= (first (mk-pair red 2)) c)))

(assert (not ((_ is cons) (cons 2 (as list-nil (MyList Int))))))
