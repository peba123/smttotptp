(set-logic UFLIA)

;; Can play with these
;; (set-option :expand-ite false)
;; (set-option :expand-distinct false)
(declare-sort Color 0)
(declare-const red Color)
(declare-const green Color)
(declare-const blue Color)
(declare-const nr-colors Int)

(assert (= nr-colors 3))
(assert (distinct red green blue))

(define-fun Int-to-Color ((i Int)) Color
  (ite (= i 0) red (ite (= i 1) green blue)))

(define-fun Color-to-Int ((c Color)) Int
  (ite (= c red) 0 (ite (= c green) 1 2)))

;; Color-to-int is the inverse of Int-to-Color on the domain [0..[nr-colors

(assert 
 (not (forall ((i Int))
	 (=> 
	  (and (<= 0 i) (< i nr-colors))
	  (let ((c (Int-to-Color i)))
	    (let ((k (Color-to-Int c)))
	      (= i k)))))))

(assert (let ((x (let ((y 1)) (+ y 2))))
	  (> x 3)))

(assert (let ((x (let ((y 5))
		   (forall ((b Bool)) (ite b (> y 0) (< y 1))))))
		   (= x x)))

(check-sat)
