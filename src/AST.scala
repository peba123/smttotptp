package smttotptp

import util._
import scala.collection.mutable.ListBuffer

/*
 * AST(cmds) creats an AST object whose fields
 * declaredSorts, definedSorts, declaredPFuns, declaredCFuns and definedFuns
 * represent the abstract syntax tree of cmds
 * If given, diagnosticOut is where diagnostic output goes, otherwise System.err
 */

class AST {

  // todo: repalce Exists and Forall classes by a common superclass

  case class Error(s: String) extends Exception

  /*
   * Global variables
   */
  var listDataTypeAdded = false
  var arrayDeclarationsAdded = false

  var intsAreReals = false
  // whether numerals are allowed as real
  // Set in set-logic

  /*
   * Syntactic entities
   */

  case class SortSym(name: String) {
    override def toString = name
  }

  abstract class Fun
  case class FunSym(name: String) extends Fun {
    val tlBoolSorted = false // Whether this is a term-level Bool-sorted function symbol
    override def toString = name
  }
  case class As(funSym: FunSym, sort: CSort) extends Fun {
    override def toString = funSym + ":" + sort
  }

  /*
   * Sorts
   */

  //a better account of xsort:
    //a static sort- this is formed by matching the sort of the operator to its arguments and never changes
    //a contextual sort- this takes account of some variable binding (read quantification) by applying a sort substitution
    //an expanded sort- this applies a set of sort definitions to transform any existing sort by rewriting.
    //note that the action of expanding a sort doesn't depend on the sort itself, so could be defined independently.

  //this would be useful because a lot of time is spent in xsort rewriting pre-emptively then discarding the result.

  object SortHelper {
    //presently unused.
  
    @annotation.tailrec
    final def expandDefined(sort: Sort, definedSorts: Map[SortSym, (List[SortParam], PSort)]): Sort = sort match {
      case _: SortParam => sort
      case CSort(sym, args) if (definedSorts.isDefinedAt(sym)) =>
        val (params, body) = definedSorts(sym)
        val sigma = SortSubst((params zip args).toMap)
        expandDefined(sigma(body), definedSorts)
      case PSortExpr(sym, args) if (definedSorts.isDefinedAt(sym)) =>
        val (params, body) = definedSorts(sym)
        val sigma = SortSubstP((params zip args).toMap)
        expandDefined(sigma(body), definedSorts)
      case CSort(sym, _) if !(declaredSorts.isDefinedAt(sym)) =>
        throw Error("undeclared/undefined sort: " + sym)
      case PSortExpr(sym, _) if !(declaredSorts.isDefinedAt(sym)) =>
        throw Error("undeclared/undefined sort: " + sym)
    }
  }

  abstract class Sort
  // concrete sorts
  case class CSort(sortSym: SortSym, args: List[CSort]) extends Sort {
    override def toString = sortSym + args.toMyString("", "[", ", ", "]")

    // expanded: remove all defined sorts in this by expansion with their definition
    def expanded: CSort =
      declaredSorts.get(sortSym) match {
        case None =>
          // see if sort is a defined one
          definedSorts.get(sortSym) match {
            case None => throw Error("undeclared/undefined sort: " + sortSym)
            case Some((params, body)) if (params.length == args.length) => {
              val sigma = SortSubst((params zip args).toMap)
              sigma(body).expanded
            }
            case _ => throw Error("wrong number of arguments in sort expression: " + this)
          }
        // Declared sorts:
        case Some(n) if (n == args.length) =>
          CSort(sortSym, args map { _.expanded })
        case _ => throw Error("sort used with wrong arity: " + this)
      }

    def forTemplate: String = {
      val SortSym(name) = sortSym
      if (args.isEmpty)
        name
      else
        "(%s %s)".format(name, (args map { _.forTemplate }).toMyString("", " ", ""))
    }
  }

  case object MATCHFAIL extends Exception

  // parametric sorts
  sealed abstract class PSort extends Sort {
    // compute a matcher from this to a given concrete sort "to", under the existing environment env.
    // In the application below, to is expanded
    def matcher(params: List[SortParam], to: CSort, env: Map[SortParam, CSort]): Map[SortParam, CSort]
    def expanded: PSort

    def forTemplate: String =
      this match {
        case SortParam(name) => "%" + name
        case PSortExpr(SortSym(name), Nil) => name
        case PSortExpr(SortSym(name), args) =>
          (args map { _.forTemplate }).toMyString("(%s ".format(name), " ", ")")
      }
  }

  object PSort {
    def matcherList(params: List[SortParam], from: List[PSort], to: List[CSort], env: Map[SortParam, CSort]): Map[SortParam, CSort] = {
      // println("matcherList : " + params + " " + from + " " + to + " " + env)
      var hfrom = from // we loop over from and
      var hto = to // we loop over to, simultaneously
      var henv = env // the result substitution
      while (!hfrom.isEmpty) {
        henv = hfrom.head.matcher(params, hto.head, henv)
        hfrom = hfrom.tail
        hto = hto.tail
      }
      henv
    }
  }

  case class SortParam(name: String) extends PSort {
    // Abstract member:
    def matcher(params: List[SortParam], to: CSort, env: Map[SortParam, CSort]): Map[SortParam, CSort] =
      env.get(this) match {
        case None =>
          // env does not act on this, so make a new binding
          env + (this -> to)
        case Some(s) =>
          // i.e. sigma(this) = s
          if (to == s) env else throw MATCHFAIL
      }
    def expanded = this
    override def toString = name
  }

  case class PSortExpr(sortSym: SortSym, args: List[PSort]) extends PSort {
    // Abstract member:
    def matcher(params: List[SortParam], to: CSort, env: Map[SortParam, CSort]): Map[SortParam, CSort] = {
      val CSort(toSortSym, toArgs) = to
      if (sortSym == toSortSym)
        PSort.matcherList(params, args, toArgs, env)
      else throw MATCHFAIL
    }
    // expanded: remove all defined sorts in this by expansion with their definition
    def expanded =
      declaredSorts.get(sortSym) match {
        case None =>
          // see if sort is a defined one
          definedSorts.get(sortSym) match {
            case None => throw Error("undeclared/undefined sort: " + sortSym)
            case Some((params, body)) if (params.length == args.length) => {
              val sigma = SortSubstP((params zip args).toMap)
              sigma(body).expanded
            }
            case _ => throw Error("wrong number of arguments in sort expression: " + this)
          }
        // Declared sorts:
        case Some(n) if (n == args.length) =>
          PSortExpr(sortSym, args map { _.expanded })
        case _ => throw Error("sort used with wrong arity: " + this)
      }
    override def toString = sortSym + args.toMyString("", "[", ", ", "]")
  }

  // SortSubst: mapping from sort parameters to concrete sorts,
  // can be applied to a parameteric sort to give a concrete sort
  case class SortSubst(env: Map[SortParam, CSort]) {

    override def toString = env.toList.toMyString("[", " ↦ ", "]")

    def apply(sort: PSort): CSort =
      sort match {
        case param @ SortParam(_) =>
          env.get(param) match {
            case Some(s) => s
            // case None => unknownSort // is this correct?
            case None => throw Error("free sort parameter " + param + " in sort " + sort)
          }
        case PSortExpr(sortSym, args) =>
          CSort(sortSym, (args map { this.apply(_) }))
      }
  }
  object SortSubst {
    def empty = new SortSubst(Map.empty)
  }

  case class SortSubstP(env: Map[SortParam, PSort]) {

    override def toString = env.toList.toMyString("[", " ↦ ", "]")

    def apply(sort: PSort): PSort =
      sort match {
        case param @ SortParam(_) =>
          env.get(param) match {
            case Some(s) => s
            case None => sort
            // This is not necessarily an error, param could be bound outside
            // throw Error("free sort parameter " + param + " in sort " + sort)
          }
        case PSortExpr(sortSym, args) =>
          PSortExpr(sortSym, (args map { this.apply(_) }))
      }
  }
  object SortSubstP {
    def empty = new SortSubstP(Map.empty)
  }

  // frequently used symbols and sorts
  val BoolSym = SortSym("Bool")
  val IntSym = SortSym("Int")
  val StringSym = SortSym("String")
  val RealSym = SortSym("Real")
  // Term-level boolean
  val TLBoolSym = SortSym("tlbool")
  // Arrays added during initialisation
  // val ArraySym = SortSym("Array")

  // And their associated sorts:
  val BoolSort = CSort(BoolSym, List.empty)
  val IntSort = CSort(IntSym, List.empty)
  val StringSort = CSort(StringSym, List.empty)
  val RealSort = CSort(RealSym, List.empty)
  val TLBoolSort = CSort(TLBoolSym, List.empty)

  val TrueTerm = App(FunSym("true"), Nil)
  val FalseTerm = App(FunSym("false"), Nil)

  val TLTrue = FunSym("tltrue")
  val TLFalse = FunSym("tlfalse")
  val TLTrueTerm = App(TLTrue, Nil)
  val TLFalseTerm = App(TLFalse, Nil)

  // val unknownSort = CSort(SortSym("???"), List.empty)

  // (declare-sort Pair 2)
  // (declare-sort Color 0)
  var declaredSorts = Map[SortSym, Int](
    BoolSym -> 0,
    IntSym -> 0,
    StringSym -> 0,
    //    ArraySym -> 2,
    RealSym -> 0)

  // (define-sort myInt () Int)
  // (define-sort Matrix (T) (Array T T))
  // (define-sort MyMatrix () (Matrix MyInt))
  var definedSorts = Map.empty[SortSym, (List[SortParam], PSort)]

  implicit class Terms(ts: Iterable[Term]) {
    def fvars = ts.foldLeft(Set.empty[Var])(_ ++ _.fvars)
    def symbols = ts.foldLeft(Set.empty[Fun])(_ ++ _.symbols)
  }

  implicit class Equals(es: Iterable[Equal]) {
    def dom = es map { case Equal(v, _) => v }
    def cod = es map { case Equal(_, t) => t }
  }

  /*
   * Terms
   */

  case class Attribute(key: Keyword, value: Option[SExpr]) {
    override def toString = key + (value match { case None => ""; case Some(v) => " " + v })
  }

  abstract class Term {

    def applySubst(sigma: Subst): Term

    /** The expaned sort of this, i.e. all defined sorts removed */
    def xsort(fvarsort: Map[Var, CSort]): CSort
    def xsort(): CSort = xsort(Map.empty[Var, CSort])
    // val attributes = List.empty[Attribute] // can be overridden
    var attributes = List.empty[Attribute] // can be overridden
    def withAttributes(atts: List[Attribute]) = {
      attributes = atts
      this
    }
    // The free variables in this
    val fvars: Set[Var]

    // The symbols used in this, includes built-in ones
    lazy val symbols: Set[Fun] = this match {
      case _: Var => Set()
      case Const(c) => Set() //skip literals too
      case App(f, args) => args.foldLeft(Set(f))(_ ++ _.symbols)
      case qf: Quantification => qf.body.symbols
      case Let(binds, body) => binds.flatMap(_.term.symbols).toSet ++ body.symbols
      case _ => ??? //not implemented exception will be thrown
    }

    // Elimination of boolean quantifified variables in this (by substitution)
    def boolQuantElim: Term

    // Obvious simplifications, may remove attributes
    def simplifyRaw: Term
    // Simplification preserving attributes
    def simplify = simplifyRaw.withAttributes(attributes)

    // pretty printer
    def toStringPP(indent: Int): String

    def nlspaces(n: Int) = "\n" + "".padTo(n, ' ')

    // Pretty print a list of terms with parenthesis and a separator,
    // derived from toStringPP:
    def toStringPPList(ts: List[Term], left: String, sep: String, right: String, indent: Int): String = {
      left + " " +
        (for (tss <- ts.tails; if !tss.isEmpty) yield {
          tss.head.toStringPP(indent + left.length + 1) +
            // More formulas?
            (if (!tss.tail.isEmpty) {
              nlspaces(indent - (sep.length - 1)) +
                sep + " "
            } else
              "")
        }).mkString + ")"
    }

    def iteExpanded(varSort: Map[Var, CSort]): Term = {

      def iteExpandedInner(t: Term): (Term, Option[(Var, Term, Term, Term)]) = {

        val (resTerm, resStruct) =
          t match {
            case App(FunSym("ite"), List(cond, thenTerm, elseTerm)) => {
              val v = Var("v", 0).fresh()
              (v, Some((v, cond, thenTerm, elseTerm)))
            }
            case App(fun, args) ⇒ {
              var rest = args
              var sofar = List.empty[Term]
              var (foundTerm: Term, foundStruct: Option[(Var, Term, Term, Term)]) = (t, None)
              while (!rest.isEmpty && foundStruct == None) {
                val next = rest.head
                iteExpandedInner(next) match {
                  case (s, None) => {
                    sofar = s :: sofar // Notice s, not t: s could be an ite-expanded version of t, if t is Bool-sorted
                    rest = rest.tail
                  }
                  case (s, Some(h)) => {
                    foundTerm = s
                    foundStruct = Some(h)
                  }
                }
              }
              foundStruct match {
                case None => (App(fun, sofar.reverse), None)
                case Some(h) =>
                  (App(fun, sofar.reverse ::: List(foundTerm) ::: rest.tail), Some(h))
              }
            }
            case Exists(vars, body) => (Exists(vars, body.iteExpanded(varSort ++ vars)), None)
            case Forall(vars, body) => (Forall(vars, body.iteExpanded(varSort ++ vars)), None)
            case Var(_, _) | Const(_) => (t, None)
            // case Let(_, _) => (t, None) // to be implemented properly
          }

        resStruct match {
          case None => (resTerm, None)
          case Some((v, cond, thenTerm, elseTerm)) => {
            // If the sort of t is Bool can expand already here
            if (t.xsort(varSort) == BoolSort) {
              // Can do the expansion already here
              val sigmaThen = Subst(Map(v -> thenTerm))
              val sigmaElse = Subst(Map(v -> elseTerm))
              // The result term may require further ite expansion
              iteExpandedInner(App(FunSym("and"),
                List(App(FunSym("implies"), List(cond, sigmaThen(resTerm))),
                  App(FunSym("implies"), List(App(FunSym("not"), List(cond)), sigmaElse(resTerm))))))
            } else
              // Defer to caller
              (resTerm, resStruct)
          }
        }
      }

      // body of iteExpanded; want toi preserve attributes
      iteExpandedInner(this)._1.withAttributes(attributes)
    }

    /*
     * Expand all let-expressions in this, which is assumed to be of Sort Bool
     * At present, only let-binders over variable v=t are supported where the sort of t is not Bool
     * This is, because let is expanded into an existentially quantified formula, and if the sort of t
     * is Bool this leads to a quantified variable of sort Bool, which is not supported by TPTP.
     */

    def letExpanded(varSort: Map[Var, CSort]): Term = {

      assume(this.xsort(varSort) == BoolSort)

      def combineBindings(b1: List[(Var, Term)], b2: List[(Var, Term)]) = {
        val (vars1, terms1) = b1.unzip
        val (vars2, terms2) = b2.unzip
        (vars1 ::: vars2) zip (terms1 ::: terms2)
      }

      def letExpandedInner(t: Term, varSort: Map[Var, CSort]): (List[(Var, Term)], Term) = {
        // First we expand all lets in all (non-proper) subterms of t:
        val (resBindings, res) =
          t match {
            case Let(bindings, body) ⇒ {
              // println("xx " + Let(bindings, body))
              // Need to rename the variables in order to avoid unintended capturing later,
              // when the binding is lifted to outer positions
              val rho = Term.mkRenaming(bindings.dom)
              val bindingsVarSort = bindings map { case Equal(v, t) => (rho(v).asInstanceOf[Var], t.xsort(varSort)) }
              val (bodyLetExpandedBindings, bodyLetExpanded) = letExpandedInner(rho(body), varSort ++ bindingsVarSort)
              val newBindings = bindings map { case Equal(v, t) => (rho(v).asInstanceOf[Var], t) }
              // println("xx " + (newBindings ::: bodyLetExpandedBindings, bodyLetExpanded))
              (newBindings ::: bodyLetExpandedBindings, bodyLetExpanded)
            }
            case App(fun, args) ⇒ {
              var argsBindings = List.empty[(Var, Term)]
              var argsRes = List.empty[Term]
              for (arg <- args) {
                val (argBindings, argRes) = letExpandedInner(arg, varSort)
                argsRes ::= argRes
                argsBindings = combineBindings(argsBindings, argBindings)
              }
              (argsBindings, App(fun, argsRes.reverse))
            }
            case Exists(vars, body) => (List.empty, Exists(vars, body.letExpanded(varSort ++ vars)))
            case Forall(vars, body) => (List.empty, Forall(vars, body.letExpanded(varSort ++ vars)))
            case Var(_, _) | Const(_) => (List.empty, t)
          }
        if (resBindings.isEmpty)
          (List.empty, res) // nothing to do - pass on result
        else {
          // Next we see if t is Bool-sorted. If so, introduce the existential quantification for resBindings now, if there are any
          if (t.xsort(varSort) == BoolSort) {
            // Yes, can build that formula already now
            // Handle the bindings with BoolSort explicitly by substitution, as TPTP cannot handle this case
            val (boolResBindings, nonBoolResBindings) = resBindings partition { case (_, t) => t.xsort(varSort) == BoolSort }
            // Expand the Bool-sorted variables
            val sigma = Subst(boolResBindings.toMap)
            val hres = sigma(res)
            // Quantify the non-Bool sorted variables, if any
            // In any case, the resulting formula may still contain let-terms,
            // stemming from terms bound to let-variables. Examples:
            // (assert (let ((x (let ((y 1)) (+ y 2)))) (> x 3)))
            // (assert (let ((x (let ((y 5)) (> y 0)))) (and x x)))
            // In both cases the variable y has not been eliminated yet.
            if (nonBoolResBindings.isEmpty)
              (List.empty, hres.letExpanded(varSort))
            else {
              val nonBoolBindingsVarSort = nonBoolResBindings map { case (v, t) => (v, t.xsort(varSort)) }
              (List.empty, // all bindings removed
                Exists(nonBoolBindingsVarSort,
                  App(FunSym("and"),
                    (nonBoolResBindings map { case (v, t) => App(FunSym("="), List(v, t)) }) :::
                      List(hres))).letExpanded(varSort))
            }
          } else {
            // t is non-Bool sorted - need to defer to caller
            (resBindings, res)
          }
        }
      }

      // body of letExpanded. Want to preserve given attributes
      letExpandedInner(this, varSort)._2.withAttributes(attributes)
    }
  }

  object Term {
    // get the singleton sort of a list of terms, if it exists
    def singletonSortOf(ts: List[Term], varSort: Map[Var, CSort]) =
      // lean implementation, always linear, could be improved of course
      (ts map { _.xsort(varSort) }).distinct match {
        case List(sort) => Some(sort)
        case _ => None
      }
    // Create a renaming substition from vars to a set of fresh variables
    def mkRenaming(vars: Iterable[Var]) =
      new Subst((vars map { x ⇒ (x, x.fresh()) }).toMap)

    private var ctr = 0
    def nextCtr() = {
      ctr += 1;
      ctr
    }

    private var funCtr = 0
    def genFunSym(prefix: String, boolSorted: Boolean) = {
      funCtr += 1
      new FunSym(prefix + "_" + funCtr) {
        override val tlBoolSorted = boolSorted 
      }
    }

  }

  // literal constants: re-use SpecConstant, for simplicity
  case class Const(const: SpecConstant) extends Term {
    def xsort(varSort: Map[Var, CSort]) = const match {
      case Numeral(_) => IntSort
      case Decimal(_) => RealSort
      case StringSExpr(_) => StringSort
    }
    val fvars = Set.empty[Var]
    def simplifyRaw = this
    def applySubst(sigma: Subst) = this
    override def toString = const.toString
    def toStringPP(indent: Int) = this.toString
    def boolQuantElim = this
  }

  case class Var(baseName: String, index: Int) extends Term {
    //why not just expand the sort at the top level?
    def xsort(varSort: Map[Var, CSort]) =
      varSort(this) /*match {
        case None => throw Error("variable has no sort: " + this)
        case Some(sort) => sort.expanded
      }*/

    val fvars = Set(this)
    def simplifyRaw = this
    def applySubst(sigma: Subst) = sigma.env.getOrElse(this, this)

    // return a fresh variable based on this one
    def fresh() = Var(baseName, Term.nextCtr())

    override def toString = baseName + (if (index == 0) "" else "_" + index)
    def toStringPP(indent: Int) = this.toString
    def boolQuantElim = this
  }


  case class App(fun: Fun, args: List[Term]) extends Term {

    // test whether the (expanded) sorts of the list of terms ts is the same as the (expanded)
    // list of given sorts
    private def haveSorts(ts: List[Term], sorts: List[CSort], varSort: Map[Var, CSort]) =
      Time("haveSorts")((ts map { _.xsort(varSort) }) == (sorts map { _.expanded }))

    lazy val fvars = args.fvars

    def simplifyRaw = {
      val argsSimplified = args map { _.simplify }
      fun match {
        case FunSym("not") => argsSimplified match {
          case List(TrueTerm) => FalseTerm
          case List(FalseTerm) => TrueTerm
          case _ => App(fun, argsSimplified)
        }
        case FunSym("and") => {
          val h = argsSimplified filterNot { _ == TrueTerm }
          if (h.isEmpty)
            TrueTerm
          else if (h contains FalseTerm)
            FalseTerm
          else
            App(fun, h)
        }
        case FunSym("or") => {
          val h = argsSimplified filterNot { _ == FalseTerm }
          if (h.isEmpty)
            FalseTerm
          else if (h contains TrueTerm)
            TrueTerm
          else
            App(fun, h)
        }
        case FunSym("implies") | FunSym("=>") => {
          // Analyse premise and conclusion of implication in the obvious way
          val (premise, conclusion) = (argsSimplified.dropRight(1), argsSimplified.last)
          val h = premise filterNot { _ == TrueTerm }
          if (h.isEmpty)
            conclusion
          else if (h contains FalseTerm)
            TrueTerm
          else
            App(fun, h :+ conclusion)
        }
        case FunSym("ite") => {
          val List(cond, thenTerm, elseTerm) = argsSimplified
          cond match {
            case FalseTerm => elseTerm
            case TrueTerm => thenTerm
            case _ => App(fun, argsSimplified)
          }
        }
        // todo: more cases: xor, distinct
        case _ => App(fun, argsSimplified)
      }
    }

    def xsort(varSort: Map[Var, CSort]) = Time("App.xsort")((fun, args) match {
      // true, false
      case (FunSym("true"), Nil) | (FunSym("false"), Nil) => BoolSort
      // not
      case (FunSym("not"), List(arg)) => {
        if (arg.xsort(varSort) != BoolSort) throw Error("ill-sorted term1: " + this)
        BoolSort
      }
      // => (right-associative)
      case (FunSym(op), args) if op == "=>" || op == "implies" => {
        if (args.length < 2)
          throw Error("not enough arguments: " + this)
        Term.singletonSortOf(args, varSort) match {
          case Some(BoolSort) => BoolSort
          case _ => throw Error("ill-sorted term2: " + this)
        }
      }
      // and, or, xor (left-associative)
      case (FunSym(op), args) if op == "and" || op == "or" || op == "xor" => {
        if (op == "xor" && args.length < 2)
          throw Error("not enough arguments: " + this)
        Term.singletonSortOf(args, varSort) match {
          case Some(BoolSort) => BoolSort
          case _ => throw Error("ill-sorted term3: " + this)
        }
      }
      // = (chainable)
      case (FunSym("="), args) => {
        if (args.length < 2)
          throw Error("not enough arguments: " + this)
        Term.singletonSortOf(args, varSort) match {
          case Some(_) => BoolSort
          case _ => throw Error("ill-sorted equation: " + this)
        }
      }
      // distinct (pairwise)
      case (FunSym("distinct"), args) => {
        if (args.length < 2)
          throw Error("not enough arguments: " + this)
        Term.singletonSortOf(args, varSort) match {
          case Some(_) => BoolSort
          case _ => throw Error("ill-sorted equation: " + this)
        }
      }
      // ite
      case (FunSym("ite"), List(cond, thenTerm, elseTerm)) => {
        if (cond.xsort(varSort) != BoolSort)
          throw Error("ill-sorted condition in ite-term: " + this)
        val thenTermSort = thenTerm.xsort(varSort)
        if (elseTerm.xsort(varSort) != thenTermSort)
          throw Error("different sorts of then-term and else-term: " + this)
        thenTermSort
      }

      // Ints/Reals
      // unary -
      case (FunSym("-"), List(arg)) => {
        arg.xsort(varSort) match {
          case IntSort => IntSort
          case RealSort => RealSort
          case _ => throw Error("ill-sorted term6: " + this)
        }
      }
      // abs
      case (FunSym("abs"), List(arg)) => {
        arg.xsort(varSort) match {
          case IntSort => IntSort
          case _ => throw Error("ill-sorted term7: " + this)
        }
      }
      // mod
      case (FunSym("mod"), List(arg1, arg2)) => {
        (arg1.xsort(varSort), arg2.xsort(varSort)) match {
          case (IntSort, IntSort) => IntSort
          case (_, _) => throw Error("ill-sorted term8: " + this)
        }
      }
      // Other Ints/Reals function symbols, all (left-)associative, hence varyadic
      case (FunSym(f), args) if f == "+" || f == "-" || f == "*" || f == "div" || f == "/" => {
        if (args.length < 2)
          throw Error("not enough arguments: " + this)
        Term.singletonSortOf(args, varSort) match {
          case Some(IntSort) if f != "/" => IntSort
          case Some(RealSort) if f != "div" => RealSort
          case _ => throw Error("ill-sorted term9: " + this)
        }
      }
      // Other Ints/Reals relation symbols, all chainable, hence varyadic
      case (FunSym(f), args) if f == ">" || f == "<" || f == ">=" || f == "<=" => {
        if (args.length < 2)
          throw Error("not enough arguments: " + this)
        Term.singletonSortOf(args, varSort) match {
          case Some(IntSort) | Some(RealSort) => BoolSort
          case _ => throw Error("ill-sorted term10: " + this)
        }
      }

      // applications of defined or declared functions
      case (fun @ FunSym(_), args) => {
        // mk-pair[T1, T2]: T1 × T2 ↦ Pair[T1, T2]
        // first[T1, T2]: Pair[T1, T2] ↦ T1
        // c: Pair[Int, Int]
        // first(c1)
        if (declaredCFuns.isDefinedAt(fun)) {
          val (argsSorts, resSort) = declaredCFuns(fun)
          if (!(haveSorts(args, argsSorts, varSort)))
            throw Error("sort mismatch in declared function application: " + this)
          resSort.expanded
        } else if (declaredPFuns.isDefinedAt(fun)) {
          val (sortParams, argsSorts, resSort) = declaredPFuns(fun)
          try {
            val env = PSort.matcherList(sortParams,
                (argsSorts map { _.expanded }), (args map { _.xsort(varSort) }), Map.empty[SortParam, CSort])
            (SortSubst(env)(resSort)).expanded
          } catch {
            case MATCHFAIL =>
              throw Error("sort mismatch in polymorphic function application: " + this)
          } 
        } else if (declaredPBoolFuns.isDefinedAt(fun)) {
          val (sortParams, argsSorts) = declaredPBoolFuns(fun)
          try {
            val env = PSort.matcherList(sortParams,
                (argsSorts map { _.expanded }), (args map { _.xsort(varSort) }), Map.empty[SortParam, CSort])
            BoolSort
          } catch {
            case MATCHFAIL =>
              throw Error("sort mismatch in polymorphic function application: " + this)
          }
        } else if (definedFuns.isDefinedAt(fun)) {
          val (funVarSort, resSort, _, _) = definedFuns(fun)
          if (!(haveSorts(args, funVarSort.values.toList, varSort)))
            throw Error("sort mismatch in defined function application: " + this)
            // don't need to type check body, have done this when function was defined
            /*            val resSortx = resSort.expanded
             if (body.xsort(varSort ++ funVarSort) != resSortx)
             throw Error("sort of body of defined function and declared sort differ: " + this)
             */
          resSort.expanded
        } else throw Error("symbol not a defined/declared function: " + this)
      }

      case (As(funSym, asSort), args) => {
        args foreach { _.xsort(varSort) } // sanity check
        asSort
        // not needed: sort has been expanded when As was created
        // asSort.expanded
      }
    })

    def applySubst(sigma: Subst) =
      // Pre-test: is it worth it?
      if ((fvars & sigma.dom).isEmpty)
        this
      else
        App(fun, args map { _.applySubst(sigma) })

    def boolQuantElim = App(fun, args map { _.boolQuantElim }).withAttributes(attributes)

    override def toString = fun match {
      case FunSym(name) if List("=", "<", ">") contains name => args.toMyString("(", " " + name + " ", ")")
      case FunSym("distinct") => args.toMyString("(", " ≠ ", ")")
      case FunSym("<=") => args.toMyString("(", " ≤ ", ")")
      case FunSym(">=") => args.toMyString("(", " ≥ ", ")")
      case FunSym("-") if args.length == 1 => "-" + args.head // unary minus
      case FunSym(name) if List("+", "-", "*", "div", "/") contains name => args.toMyString("(", " " + name + " ", ")")
      case FunSym("not") => "¬" + args.head
      case FunSym(name) if List("=>", "implies") contains name => args.toMyString("(", " ⇒ ", ")")
      case FunSym("and") => args.length match {
        case 0 => "true"
        case 1 => args(0).toString
        case _ => args.toMyString("(", " ∧ ", ")")
      }
      case FunSym("or") => args.length match {
        case 0 => "false"
        case 1 => args(0).toString
        case _ => args.toMyString("(", " ∨ ", ")")
      }
      case FunSym("xor") => args.toMyString("(", " xor ", ")")
      case FunSym("ite") => "(if " + args(0) + " " + args(1) + " else " + args(2) + ")"
      case FunSym("select") => args(0) + "[" + args(1) + "]"
      case FunSym("store") => "(" + args(0) + "[" + args(1) + "] ← " + args(2) + ")"
      // default is prefix, as expected
      case _ => fun + args.toMyString("", "(", ", ", ")")
    }

    // toString pretty printed
    def toStringPP(indent: Int) = fun match {
      case FunSym(name) if List("=", "<", ">") contains name => args.toMyString("(", " " + name + " ", ")")
      case FunSym("distinct") => args.toMyString("(", " ≠ ", ")")
      case FunSym("<=") => args.toMyString("(", " ≤ ", ")")
      case FunSym(">=") => args.toMyString("(", " ≥ ", ")")
      case FunSym("-") if args.length == 1 => "-" + args.head // unary minus
      case FunSym(name) if List("+", "-", "*", "div", "/") contains name => args.toMyString("(", " " + name + " ", ")")
      case FunSym("not") => "¬" + args.head.toStringPP(indent + 1)
      case FunSym(name) if List("=>", "implies") contains name => toStringPPList(args, "(", "⇒", ")", indent)
      case FunSym("and") => args.length match {
        case 0 => "true"
        case 1 => args(0).toStringPP(indent)
        case _ => toStringPPList(args, "(", "∧", ")", indent)
      }
      case FunSym("or") => args.length match {
        case 0 => "false"
        case 1 => args(0).toStringPP(indent)
        case _ => toStringPPList(args, "(", "∨", ")", indent)
      }
      case FunSym("xor") => toStringPPList(args, "(", "xor", ")", indent)
      case FunSym("ite") =>
        "(if  " + args(0).toStringPP(indent + 5) +
          nlspaces(indent + 5) + args(1).toStringPP(indent + 5) +
          nlspaces(indent + 5) + "else " + args(2).toStringPP(indent + 5) + ")"
      case FunSym("select") => args(0) + "[" + args(1) + "]"
      case FunSym("store") => "(" + args(0) + "[" + args(1) + "] ← " + args(2) + ")"
      // default is prefix, as expected
      case _ => fun + args.toMyString("", "(", ", ", ")")
    }
  }

  abstract class Quantification extends Term {
    val name: String
    val vars: List[(Var, CSort)]
    val body: Term

    def xsort(varSort: Map[Var, CSort]) = {
      val quantVarSort = Map.empty[Var, CSort] ++ vars
      body.xsort(varSort ++ quantVarSort) // bindings in quantVarSort override the ones in varSort
    }

    lazy val fvars = body.fvars -- ((vars map { _._1 }).toSet)

    override def toString = name + " " + (vars map { vs => vs._1 + ":" + vs._2 }).toMyString("", " ", "") + " " + body

    def applySubst(sigma: Subst, resFn: (List[(Var, CSort)], Term) => Quantification) = {
      // We need to keep only the bindings for the free variables in this
      val sigma1 = sigma.removeBindings(sigma.dom -- fvars)
      if (sigma1.isEmpty) this // why bother?
      else {
        val boundVars = vars map { _._1 }
        if ((sigma1.varCod & boundVars.toSet).isEmpty)
          // Will not capture variables in codom of sigma1
          resFn(vars, sigma(body))
          // Exists(vars, sigma(body))
        else {
          // Rename the bound variables before applying sigma to avoid name capturing
          val rho = Term.mkRenaming(boundVars)
          resFn(vars map { case (v, s) => (rho(v).asInstanceOf[Var], s) }, sigma(rho(body)))
          // Exists(vars map { case (v, s) => (rho(v).asInstanceOf[Var], s) }, sigma(rho(body)))
        }
      }
    }

    def toStringPP(indent: Int) =
      name + " " + (vars map { vs => vs._1 + ":" + vs._2 }).toMyString("", " ", "") +
        nlspaces(indent + 2) + body.toStringPP(indent + 2)

  }

  case class Exists(vars: List[(Var, CSort)], body: Term) extends Quantification {
    val name = "∃"
    def applySubst(sigma: Subst) = applySubst(sigma, Exists(_, _))

    def simplifyRaw = Exists(vars, body.simplify)

    def boolQuantElim = {
      // Substitute all variables in vars by true and by false and or these results
      @annotation.tailrec
      def boolQuantElimInner(vars: List[(Var, CSort)], body: Term): Term = {
        vars match {
          case Nil => body
          case (v, _BoolSort) :: rest => {
            val substTrue = Subst(Map(v -> TrueTerm))
            val substFalse = Subst(Map(v -> FalseTerm))
            boolQuantElimInner(rest, App(FunSym("or"), List(substTrue(body), substFalse(body))).simplify)
          }
        }
      }

      val bodyQuantEliminated = body.boolQuantElim
      val (boolVars, nonBoolVars) = vars partition { _._2 == BoolSort }
      val res = 
        if (boolVars.isEmpty)
          // nonBoolVars cannot be empty
          Exists(nonBoolVars, bodyQuantEliminated)
        else {
          val resBody = boolQuantElimInner(boolVars, bodyQuantEliminated)
          if (nonBoolVars.isEmpty)
            resBody
          else
            Exists(nonBoolVars, resBody)
        }
      res.withAttributes(attributes) // preserve given attributes
    }
  }

  case class Forall(vars: List[(Var, CSort)], body: Term) extends Quantification {
    val name = "∀"
    def applySubst(sigma: Subst) = applySubst(sigma, Forall(_, _))

    def simplifyRaw = Forall(vars, body.simplify)

    def boolQuantElim = {
      // Substitute all variables in vars by true and by false and and these results
      @annotation.tailrec
      def boolQuantElimInner(vars: List[(Var, CSort)], body: Term): Term = {
        vars match {
          case Nil => body
          case (v, _BoolSort) :: rest => {
            val substTrue = Subst(Map(v -> TrueTerm))
            val substFalse = Subst(Map(v -> FalseTerm))
            boolQuantElimInner(rest, App(FunSym("and"), List(substTrue(body), substFalse(body))).simplify)
          }
        }
      }

      val bodyQuantEliminated = body.boolQuantElim
      val (boolVars, nonBoolVars) = vars partition { _._2 == BoolSort }
      if (boolVars.isEmpty)
        // nonBoolVars cannot be empty
        Forall(nonBoolVars, bodyQuantEliminated)
      else {
        val resBody = boolQuantElimInner(boolVars, bodyQuantEliminated)
        if (nonBoolVars.isEmpty)
          resBody
        else
          Forall(nonBoolVars, resBody)
      }
    }

    // Old version - not so good
    // def boolQuantElim() = {
    //   val (boolVars, nonBoolVars) = vars partition { _._2 == BoolSort }
    //   boolVars match {
    //     case Nil => Forall(nonBoolVars, body.boolQuantElim())
    //     case v :: rest => { 
    //       val substTrue = Subst(Map(v._1 -> TrueTerm))
    //       val substFalse = Subst(Map(v._1 -> FalseTerm))
    //         (rest ::: nonBoolVars) match {
    //         case Nil => App(FunSym("and"), List(substTrue(body), substFalse(body))).boolQuantElim()
    //         case vars => Forall(vars, App(FunSym("and"), List(substTrue(body), substFalse(body)))).boolQuantElim()
    //       }
    //     }
    //   }
    // }
  }

  case class Equal(v: Var, term: Term) {
    override def toString = v + "=" + term
  }

  def bindingsToVarSort(bindings: List[Equal], varSort: Map[Var, CSort]) =
    Map.empty[Var, CSort] ++ (bindings map { case Equal(v, term) => (v -> term.xsort(varSort)) })

  case class Let(bindings: List[Equal], body: Term) extends Term {
    def xsort(varSort: Map[Var, CSort]) = {
      body.xsort(varSort ++ bindingsToVarSort(bindings, varSort)) // bindings in letVarSort override the ones in varSort
    }

    // free variables is a bit tricky:
    // fv(let x = x+1 in x+y) = {x, y}
    // fv(let x = (let y = x+1 in y+1) in x+y) = {x, y}

    lazy val fvars = (body.fvars -- bindings.dom) ++ bindings.cod.fvars

    def simplifyRaw = Let(bindings map { case Equal(v, t) => Equal(v, t.simplify) }, body.simplify)

    def applySubst(sigma: Subst) = {
      val sigmaForBindings = sigma.removeBindings(sigma.dom -- fvars)
      if (sigmaForBindings.isEmpty) this // why bother?
      else {
        // sigmaForBindings might contain bindings for (free) variables
        // that are also bound by this, like x above in the example
        // These bindings have to be removed.
        val boundVars = bindings.dom
        val sigmaForBody = sigmaForBindings.removeBindings(boundVars)
        if ((sigmaForBody.varCod & boundVars.toSet).isEmpty)
          // Will not capture variables in codom of sigmaForBody
          Let(bindings map { case Equal(v, t) => Equal(v, sigmaForBindings(t)) },
            sigmaForBody(body))
        else {
          val rho = Term.mkRenaming(boundVars)
          Let(bindings map { case Equal(v, t) => Equal(rho(v).asInstanceOf[Var], sigmaForBindings(t)) },
            sigmaForBody(rho(body)))
        }
      }
    }
    override def toString = "let " + bindings.toMyString("", ", ", "") + " in " + body

    // boolQuantElim does not need to look at boolean variables among the bound
    // variables, these are eliminated by let-elimination (earlier)
    def boolQuantElim = {
      Let(bindings map { case Equal(v, t) => Equal(v, t.boolQuantElim) },
        body.boolQuantElim)
    }


    def toStringPP(indent: Int) = "let " + bindings.toMyString("", ", ", "") + " in" +
      nlspaces(indent + 2) + body.toStringPP(indent + 2)
  }

  case class MatchCase(pattern: Term, term: Term) {
    override def toString = "case " + pattern + " => " + term
  }


  case class Match(target: Term, cases: List[MatchCase]) extends Term {
    def applySubst(sigma: AST.this.Subst): AST.this.Term = ???
    def boolQuantElim: AST.this.Term = ???
    val fvars: Set[AST.this.Var] = ???
    def simplifyRaw: AST.this.Term = ???
    def toStringPP(indent: Int): String = ???
    def xsort(fvarsort: Map[AST.this.Var,AST.this.CSort]): AST.this.CSort = ???
  }


  /*
   * Term substitutions
   */

  // SortSubst: mapping from sort parameters to concrete sorts,
  // can be applied to a parameteric sort to give a concrete sort
  case class Subst(env: Map[Var, Term]) {
    override def toString = env.toList.toMyString("[", " ↦ ", "]")
    def apply(t: Term): Term = t.applySubst(this)

    def removeBindings(vars: Iterable[Var]) = Subst(env -- vars)
    def isEmpty = env.isEmpty
    lazy val dom = env.keySet
    lazy val varCod = (env.values flatMap { _.fvars }).toSet
  }

  object Subst {
    def empty = new Subst(Map.empty)
  }

  // (declare-fun f (Int MyInt (Array Int)) Int) 
  var declaredCFuns = Map.empty[FunSym, (List[CSort], CSort)]

  // The List[Symbol] component below contains the sort parameters.
  // The SMT-Lib language does not support them, but they creep in with Z3 declare-z3-datatype
  // as these lead to polymorphic functions. E.g.
  // (declare-z3-datatype (T1 T2) ((Pair (mk-pair (first T1) (second T2)))))
  // Leads to
  // (declare-sort Pair 2)
  // (declare-fun mk-pair (T1 T2) (T1 T2) (Pair T1 T2))
  // (declare-fun first (T1 T2) ((Pair T1 T2) T1))
  // (declare-fun second (T1 T2) ((Pair T1 T2) T2))
  var declaredPFuns = Map.empty[FunSym, (List[SortParam], List[PSort], PSort)]

  var declaredPBoolFuns = Map.empty[FunSym, (List[SortParam], List[PSort])]

  // (define-fun f ((i Int) (j (Array Int))) myInt (+ i i))
  var definedFuns = Map.empty[FunSym, (Map[Var, CSort], CSort, Term, String)]

  // What is given in a set-logic command, if at all
  var logic: Option[String] = None

  // The options given in the input file
  var options = List.empty[Attribute]

  // The informations given in the input file, via set-info
  var infos = List.empty[Attribute]

  def getOptionStringValue(s: String) =
    (options find { _.key == Keyword(s) }) match {
      case Some(Attribute(_, Some(Symbol(value)))) => Some(value)
      case _ => None
    }

  def allDistinct(sexprs: List[SExpr]): Boolean = {
    for (
      ss <- sexprs.tails;
      if !ss.isEmpty
    ) if (ss.tail exists { _ == ss.head })
      return false
    return true
  }

  def variablesOK(vbs: List[SExpr]) =
    // check that vbs is comprised of pairs of variables and well-sorted terms
    (vbs forall {
      case Pair(Symbol(_), _) => true
      case _ => false
    }) &&
      // check that all variables are distinct
      allDistinct(vbs map { case Pair(v, _) => v })

  /*
   * parseCSort, parsePSort: parsing of sort expressions
   * parseCSort expands all defined sorts
   */

  def parseCSort(s: SExpr): CSort =
    // Do we need to expand sorts?
    s match {
      case Symbol(name) => CSort(SortSym(name), List.empty).expanded
      case SExprList((Symbol(name)) :: args) =>
        CSort(SortSym(name), args map { parseCSort(_) }).expanded
    }

  // parsing a parametric sort requires a list of declared sort parameters, to recognize these
  def parsePSort(s: SExpr, params: List[Symbol]): PSort =
    s match {
      case s @ Symbol(name) =>
        // could be a parameter or a defined or declared 0-ary sort, but we do not check the latter here
        if (params contains s)
          SortParam(name)
        else
          PSortExpr(SortSym(name), List.empty)
      case SExprList(Symbol(name) :: args) =>
        PSortExpr(SortSym(name), args map { parsePSort(_, params) })
    }

  def toVarSort(sortedVars: List[SExpr]) = {
    val qVarsVars = sortedVars map { case Pair(Symbol(name), _) => Var(name, 0) }
    val qVarsSorts = sortedVars map { case Pair(_, s) => parseCSort(s) }
    (qVarsVars zip qVarsSorts)
  }

  /*
 * parseTerm:
 * transform a term in concrete syntax into on in abstract syntax.
 * No sort checking is done. Parameter vars is a list of symbols that are the currently known variables.
 * This is needed because in the concrete syntax, without context, variables would be indistinguishable
 *  from constants.
   * Augsut 2023: Also include a mapping from vars to their CSorts, needed for match expression expansion 
 */
  // todo: get rid of vars, its redundant with varSort
  def parseTerm(s: SExpr, varSort: Map[Var, CSort]): Term = s match {

    // term with attributes
    case SExprList(Symbol("!") :: t :: annotations) => {
      // Parse the annotations into attributes
      var att = List.empty[Attribute]
      var ann = annotations
      while (!ann.isEmpty) {
        val key = (ann.head match {
          case k @ Keyword(_) => k
          case _ => throw Error("Keyword expected in annotations of term: " + s)
        })
        ann = ann.tail
        val value =
          if (ann.isEmpty)
            None
          else (ann.head match {
            case k @ Keyword(_) => None // got the next attribute already
            case e => { ann = ann.tail; Some(e) }
          })
        att ::= Attribute(key, value)
      }
      att = att.reverse
      // parse the term proper and add attributes. This is a bit clumsy.
      val res = 
        parseTerm(t, varSort) match {
          case Const(const) => new Const(const)
          case Var(baseName, index) => new Var(baseName, index)
          case App(fun, args) => new App(fun, args)
          case Exists(vars, body) => new Exists(vars, body)
          case Forall(vars, body) => new Forall(vars, body)
          case Let(bindings, body) => new Let(bindings, body)
        }
      res.attributes = att
      res
/*
      parseTerm(t, vars) match {
        case Const(const) => new Const(const) { override attributes = att }
        case Var(baseName, index) => new Var(baseName, index) { override val attributes = att }
        case App(fun, args) => new App(fun, args) { override val attributes = att }
        case Exists(vars, body) => new Exists(vars, body) { override val attributes = att }
        case Forall(vars, body) => new Forall(vars, body) { override val attributes = att }
        case Let(bindings, body) => new Let(bindings, body) { override val attributes = att }
      }
 */
    }

    case const @ Numeral(n) => if (intsAreReals) Const(Decimal(RatInt(n))) else Const(const)
    case const @ Decimal(_) => Const(const)
    case const @ StringSExpr(_) => Const(const)
    case sym @ Symbol(name) =>
      if (varSort.keySet contains Var(name, 0)) Var(name, 0) else App(FunSym(name), List.empty)

    case SExprList(List(Symbol("as"), Symbol(name), sortExpr)) =>
      App(As(FunSym(name), SortSubst.empty(parsePSort(sortExpr, List.empty).expanded)), List.empty)

    case SExprList(List(Symbol("let"), SExprList(varBindings), body)) => {
      if (!variablesOK(varBindings))
        throw Error("ill-formed bindings in let-term: " + s)
      val letVars = varBindings map { case Pair(Symbol(name), _) => Var(name, 0) }
      val letTerms = varBindings map { case Pair(_, term) => parseTerm(term, varSort) }
      val letTermsSorts = letTerms map { _.xsort(varSort) }
      val letBindings = (letVars zip letTerms) map { case (v, t) => Equal(v, t) }
      // val letBody = parseTerm(body, vars ++ (varBindings map { case Pair(v, _) => v.asInstanceOf[Symbol] }).toSet)
      // SImpler
      val letBody = parseTerm(body, varSort ++ (letVars zip letTermsSorts))
      // bindings in newVars override the ones in vars
      Let(letBindings, letBody)
    }
    case SExprList(List(Symbol("match"), target, SExprList(cases))) => {
      // println(s"match statement: $s")
      // val s = parseCSort(target)
      // println(s"target term sort: $s")
      val targetSortSym = parseTerm(target, varSort).xsort(varSort).sortSym
      // println(s"target sort symbol: $targetSortSym")
      // println(s"sortDestNames: $sortDestNames")
      val destNames = sortDestNames(targetSortSym)
      // println(s"Destructor for sort ${targetSortSym}: $destNames")
      var iteCases = List.empty[(SExpr, SExpr)]
      var varCase = false
      for (matchCase <- cases) {
        val Pair(pattern, caseBody) = matchCase
        // Convert to building block for ite
        val (cond, body) = 
          pattern match {
            case const @ Symbol(name) if (destNames contains const) =>
              if (destNames(const).nonEmpty)
                throw Error(s"Not a null-ary constructor for $targetSortSym: $const")
              (SExprList(List(Symbol(PBParser.indexedName("is", Seq(name))), target)),
               caseBody
              )
            case const @ Symbol(name) =>
              // case of a variable
              (Symbol("true"),
               SExprList(List(Symbol("let"), SExprList(List(SExprList(List(const, target)))), caseBody)
                )
              )
            case SExprList((const @ Symbol(name)) :: vars) =>
              if (!(destNames contains const)) throw Error(s"Not a constructor for $targetSortSym: $const")
              if (! (vars forall { _.isInstanceOf[Symbol] })) 
                throw Error(s"All arguments of constructor $const in match expression must by symbols: $vars")
              val dests = destNames(const) // e.g. [head, tail] for cons
              if (vars.length != dests.length) 
                throw Error(s"Number of pattern variables does not match definition of $const")
              val binders = for ((v,d) <- (vars zip dests)) yield {
                  SExprList(List(v, SExprList(List(d, target))))
                }
              (SExprList(List(Symbol(PBParser.indexedName("is", Seq(name))), target)),
                SExprList(List(Symbol("let"), SExprList(binders), caseBody)
                ))
          }
        iteCases = iteCases ::: List((cond, body))
        // println(s"cond = $cond")
        // println(s"body = $body")
      }
      // println(iteCases)
      // Turn iteCases into an SExpr as per SMT-Lib doc
      def h(iteCases: List[(SExpr, SExpr)]): SExpr =
          iteCases match {
            case (cond, body) :: Nil => body
            case (Symbol("true"), body) :: rest => body
            case (cond, body) :: rest =>
              SExprList(List(Symbol("ite"), cond, body, h(rest)))
          }
      val res = h(iteCases)
      parseTerm(res, varSort)
      // datatypeDecls foreach println
      // datatypeDefs foreach { case (sortSym, templates) => 
      //   println(s"$sortSym => ")
      //   templates foreach println
      // }
      // val matchCases = cases map {
      //     case Pair(pattern, term) => 
      //       print(s"target sort: ${parseTerm(target, varSort).xsort(varSort)}")
      //       MatchCase(parseTerm(pattern, varSort), parseTerm(term, varSort))
      //     case other => throw Error("ill-formed cases in match-term: " + s)
      //   }
      // Match(parseTerm(target, varSort), matchCases)
      // throw Error("not yet implemented")
    }

/*      if (!matchCasesOK(matchCases))
        throw Error("ill-formed cases in match-term: " + s)
      val letVars = varBindings map { case Pair(Symbol(name), _) => Var(name, 0) }
      val letTerms = varBindings map { case Pair(_, term) => parseTerm(term, vars) }
      val letBindings = (letVars zip letTerms) map { case (v, t) => Equal(v, t) }
      val letBody = parseTerm(body, vars ++ (varBindings map { case Pair(v, _) => v.asInstanceOf[Symbol] }).toSet)
      // bindings in newVars override the ones in vars
      Let(letBindings, letBody)
    }
 */

    case SExprList(List(Symbol(q), SExprList(sortedVars), body)) if q == "forall" || q == "exists" => {
      if (!variablesOK(sortedVars))
        throw Error("ill-formed variable list in quantification: " + s)
      // These two are used in the Forall/Exists constructor:
      val qVars = toVarSort(sortedVars)
      // println(s"qvars = $qVars")
      q match {
        case "forall" => Forall(qVars, parseTerm(body, varSort ++ qVars))
        case "exists" => Exists(qVars, parseTerm(body, varSort ++ qVars))
      }
    }

    case SExprList(Symbol(name) :: args) =>
      App(FunSym(name), (args map { parseTerm(_, varSort) }))

    case SExprList(SExprList(List(Symbol("as"), Symbol(name), sortExpr)) :: args) =>
      App(As(FunSym(name), SortSubst.empty(parsePSort(sortExpr, List.empty).expanded)), (args map { parseTerm(_, varSort) }))

    case _ => throw Error("cannot parse term " + s)
  }

  // The asserted formulas; first component is the current version of asserted term,
  // second component is the source term it was obtained from
  var assertions = ListBuffer.empty[(Term, Term)]

  case class Template(params: List[String], body: String) {
    // textual substitution of all params by by, pairwise
    def apply(by: List[String]) = {
      require(params.length == by.length)
      var res = body
      for ((v, t) <- params zip by)
        res = res.replaceAll(v, t)
      res
    }
    override def toString = params.toMyString("(", " ", ")") + " -> " + body
  }

  // Not needed, the parametric functions obtained from the declare-z3-datatype command are enough
  var datatypeDecls = Map.empty[SortSym, List[Template]]
  var datatypeDefs = Map.empty[SortSym, List[Template]]
  // map constructors to their destructors with arities, e.g. cons -> [head, tail] for List :
  // arity is implicit from length of list
  var sortDestNames = Map.empty[SortSym, Map[Symbol, List[Symbol]]]
  /*
 * datatypeToSMTStringFuns: generate templates for various functions:
 Selectors:
 tff(ax1, axiom, ( ! [ K: $int, L: list ] : head(cons(K, L)) = K )).
 tff(ax1, axiom, ( ! [ K: $int, L: list ] : tail(cons(K, L)) = L )).
 Distinct cases:
 tff(ax2, axiom, ( ! [ K: $int, L: list ] : cons(K, L) != nil)).
 Constructors:
 tff(ax3, axiom, ( ! [ L: list ] : 
  ( L = nil
  | L = cons(head(L), tail(L)) ))).
 */

  def datatypeToTemplates(params: List[SExpr], sortSymbol: Symbol, cases: List[SExpr]) {

    def pairsForTemplate[T1, T2](l1: List[T1], l2: List[T2]) =
      ((l1 zip l2) map { case (e1, e2) => "(%s %s)".format(e1, e2) }).toMyString("", " ", "")

    val Symbol(name) = sortSymbol
    val sortSym = SortSym(name)
    // not needed
    var destDecls = List.empty[Template]
    var constDecls = List.empty[Template]
    var destNames = Map.empty[Symbol, List[Symbol]]
    var ax1 = List.empty[Template]
    var ax2 = List.empty[Template]
    var axIs = List.empty[Template]
    var ax2cases = List.empty[(List[String], List[String], String)]
    var ax3cases = List.empty[String]
    val paramsSMT = params map { case Symbol(n) => "%" + n }
    val sortTemplate =
      if (params.isEmpty)
        name
      else
        // "(%s %s)".format(name, (params map { _ => "%s" }).toMyString("", " ", ""))
        "(%s %s)".format(name, paramsSMT.toMyString("", " ", ""))
    val sortParams = params map { case Symbol(p) => SortParam(p) }
    val sort = PSortExpr(SortSym(name), sortParams)
    // Analyse cases
    for ((c, j) <- cases zip (0 until cases.length))
      c match {
        case Symbol(name) => {
          destNames += ((Symbol(name) -> List.empty))
          val maybeCoerced = (if (params.isEmpty) name else "(as %s %s)".format(name, sortTemplate))
          ax3cases ::= maybeCoerced
          ax2cases ::= (List.empty, List.empty, maybeCoerced)
          axIs ::= Template(paramsSMT, s"(assert (forall ((X ${sortTemplate})) (= ((_ is $name) X) (= X $maybeCoerced))))")
        }
        case SExprList(Symbol(name) :: dests) => {
          var ax3casesInner = List.empty[String]
          var constParamSorts = List.empty[PSort]
          for (d <- dests)
            d match {
              case Pair(Symbol(name), destSortExpr) => {
                ax3casesInner ::= "(%s X)".format(name)
                val realDestSort =
                  if (destSortExpr == sortSymbol)
                    // Use the sort expression instead of the sort name
                    sort
                  else
                    parsePSort(destSortExpr, params.asInstanceOf[List[Symbol]]) // use what is given
                constParamSorts ::= realDestSort
                destDecls ::= Template(paramsSMT,
                "(declare-fun %s (%s) %s)".format(name, sort.forTemplate, realDestSort.forTemplate))
              }
              case _ => throw Error("ill-formed destructor definition: " + d)
            }
          destNames += ((Symbol(name), (dests map { case Pair(dest: Symbol, _) => dest })))
          ax3cases ::= ax3casesInner.reverse.toMyString("(%s ".format(name), " ", ")")
          // Declare the constructor:
          constParamSorts = constParamSorts.reverse
          val formalVars = (for (i <- 0 until dests.length) yield "X_%s_%s".format(j, i)).toList
          val formalVarsSMT = formalVars.toMyString("", " ", "")
          val constParamSortsSMT = constParamSorts map { _.forTemplate }
          ax2cases ::= ((formalVars, constParamSortsSMT, "(%s %s)".format(name, formalVarsSMT)))
          for ((Pair(Symbol(destName), destSortExpr), i) <- (dests zip (0 until dests.length)))
            ax1 ::= Template(paramsSMT, "(assert (forall (%s) (= (%s (%s %s)) %s)))".
              format(pairsForTemplate(formalVars, constParamSortsSMT),
                destName, name, formalVarsSMT, "X_%s_%s".format(j, i)))
          constDecls ::= Template(paramsSMT, "(declare-fun %s %s %s)".format(name, constParamSortsSMT.toMyString("(", " ", ")"), sort.forTemplate))
          axIs ::= Template(paramsSMT, s"(assert (forall ((X ${sortTemplate})) (= ((_ is $name) X) (exists (${pairsForTemplate(formalVars, constParamSortsSMT)}) (= X ($name $formalVarsSMT))))))")
          // println(Template(paramsSMT, s"(assert (forall ((X $sortTemplate)) (= (_ is $name X) true)))"))
          // axIs ::= Template(paramsSMT, s"(assert (forall ((X $sortTemplate)) (= ((_ is $name) X) true)))")
        }
      }
    // done with all cases

    sortDestNames += ((sortSym -> destNames))
    ax1 = ax1.reverse

    ax2cases = ax2cases.reverse
    for (
      ss <- ax2cases.tails;
      if (!ss.isEmpty);
      ts <- ss.tail.tails;
      if (!ts.isEmpty)
    ) {
      val (svars, ssorts, sterm) = ss.head
      val (tvars, tsorts, tterm) = ts.head
      ax2 ::= Template(paramsSMT,
        if (svars.isEmpty && tvars.isEmpty)
          "(assert (not (= %s %s)))".format(sterm, tterm)
        else
          "(assert (forall (%s) (not (= %s %s))))".format(
            pairsForTemplate((svars ::: tvars), (ssorts ::: tsorts)),
            sterm, tterm))
    }
    ax2 = ax2.reverse

    val ax3 = Template(paramsSMT, "(assert (forall ((X %s)) %s))".
      format(sortTemplate,
        (ax3cases.reverse map { "(= X %s)".format(_) }).toMyString("(or ", " ", ")")))

    axIs = axIs.reverse
    datatypeDefs += (sortSym -> (ax3 :: ax1 ::: ax2 ::: axIs))
    datatypeDecls += (sortSym -> (constDecls ::: destDecls))
  }

  /*
 * API methods from here on. See smttotptp.scala for usage.
 */

  def parseCommand(cmd: SExpr) {

    def isSymbol(s: SExpr) = s.isInstanceOf[Symbol]
    def isSymbols(s: List[SExpr]): Boolean = s forall { isSymbol(_) }

    //helper methods for function definition
    def symbolIsDefined(f: FunSym): Boolean =
      (declaredCFuns contains f) || (declaredPFuns contains f) || (declaredPBoolFuns contains f) || (definedFuns contains f)

    // println("Parsing command " + cmd)

    cmd match {
      // declare-z3-datatype, as in Z3
      // Example 1 - records
      // (declare-z3-datatype (T1 T2) ((Pair (mk-pair (first T1) (second T2)))))
      // Leads to
      // (declare-sort Pair 2)
      // (declare-parametric-fun (T1 T2) mk-pair (T1 T2) (Pair T1 T2))
      // (declare-parametric-fun (T1 T2) first ((Pair T1 T2) T1))
      // (declare-parametric-fun (T1 T2) second ((Pair T1 T2) T2))

      // Example 2 - enumerated types
      // (declare-z3-datatype () ((S A B C)))
      // Leads to
      // (declare-sort S 0)
      // (declare-parametric-fun () A () S)
      // (declare-parametric-fun () B () S)
      // (declare-parametric-fun () C () S)
      //
      // Example 3 - lists
      // (declare-z3-datatype (T) ((Lst nil (cons (hd T) (tl Lst)))))
      // Leads to
      // (declare-sort Lst 1)
      // (declare-parametric-fun (T) nil () (Lst T)) ;; notice free sort parameter
      // (declare-parametric-fun (T) cons (T (Lst T)) (Lst T))
      // (declare-parametric-fun (T) hd ((Lst T)) T)
      // (declare-parametric-fun (T) tl ((Lst T)) (Lst T))

      case SExprList(List(Symbol("set-logic"), Symbol(l))) => {
        if (logic != None)
          smttotptp.warning("repeated set-logic command: " + cmd)
        logic = Some(l)
        if (l.startsWith("A") || l.startsWith("QF_A")) {
          // Deal with arrays
          addArrayDeclarations()
        }
        if (l.endsWith("RA") && !l.endsWith("IRA"))
          intsAreReals = true
      }

      case SExprList(Symbol("set-option") :: (k @ Keyword(_)) :: args) => {
        options = options ::: List(Attribute(k,
          args match {
            case Nil => None
            case List(arg) => Some(arg)
            case _ => throw Error("ill-formed set-option command: " + cmd)
          }))

        // On some options we need to act immediately
        (k, args) match {
          case (Keyword(":declare-list-datatype"), List(Symbol("true"))) =>
            addListDatatype()
          case (Keyword(":declare-arrays"), List(Symbol("true"))) =>
            addArrayDeclarations()
          case _ => ()
        }
      }

      case SExprList(Symbol("set-info") :: (k @ Keyword(_)) :: args) => {
        infos = infos ::: List(Attribute(k,
          args match {
            case Nil => None
            case List(arg) => Some(arg)
            case _ => throw Error("ill-formed set-info command: " + cmd)
          }))
      }

/*
 * declare-datatypes
 * Examples from the SMTLib 2.6 manual

 *  Parametric lists 
    (declare-datatypes ( (List 1) ) (
       (par (T) ( (nil) (cons (car T) (cdr (List T)) )))))

 * translated into

    (declare-z3-datatype (T) ((List nil (cons (car T) (cdr (List T))))))

 *  Two mutually recursive datatypes 
 (declare-datatypes ( (Tree 1) (TreeList 1) ) (
     ;; Tree
     (par (X) ( (node (value X) (children (TreeList X)) ))) 
     ;; TreeList
     (par (Y) ( (empty)
           (insert (head (Tree Y)) (tail (TreeList Y))) ))))
 * translated into
 (declare-z3-datatype (X) ((Tree (node (value X) (children (TreeList X)) ))))
 (declare-z3-datatype (Y) ((TreeList empty (insert (head (Tree Y)) (tail (TreeList Y))))))

 */
      case SExprList(List(Symbol("declare-datatypes"), SExprList(nameAndArities), SExprList(decls))) => {
        val res = collection.mutable.ListBuffer.empty[SExprList]
        for ((SExprList(List(Symbol(name), Numeral(arity))), SExprList(decl) ) <- (nameAndArities zip decls)) {
          val a = arity.toInt
          val (params, cases) = a match {
              case 0 => (List.empty, decl)
              case n => decl match {
                case List(Symbol("par"), SExprList(ps), SExprList(cs)) => (ps, cs)
                case _ => throw Error("cannot parse datatypes declaration: " + decl)
              }
            }
          if (params.length != a) throw Error("arity differs from parameter list length in datatypes declaration: " + cmd)
          // zero-ary contructors are stripped their parenthesis for Z3-style declarations
          val casesZ3 = cases map {
              case SExprList(List(c)) => c
              case other => other
            }
          // Call parsecommand on the transformed 2.6 style declaration
          parseCommand(SExprList(List(
            Symbol("declare-z3-datatype"), 
            SExprList(params), 
            SExprList(List(SExprList(Symbol(name) :: casesZ3))))))
        }
      }

      case SExprList(List(Symbol("declare-datatype"), name, decl)) => {
        parseCommand(SExprList(List(Symbol("declare-datatypes"), SExprList(List(SExprList(List(name, Numeral(0))))), SExprList(List(decl)))))
      }

      case SExprList(List(Symbol("declare-z3-datatype"), SExprList(params), SExprList(SExprList((sortSymbol @ Symbol(name)) :: cases) :: more))) => {
        val sortSym = SortSym(name)
        if (!(isSymbols(params) && allDistinct(params)))
          throw Error("non-symbol or duplicate symbol in parameter list of datatype declaration: " + cmd)
        if ((declaredSorts contains sortSym) || (definedSorts contains sortSym))
          throw Error("sort already declared or defined: " + name)
        if (!more.isEmpty)
          throw Error("two or more declarations not yet handled: " + cmd)
        // Declare the new sort
        // println("xxx datatype" + (sortSym -> params.length))
        declaredSorts += (sortSym -> params.length)
        // The new paramatric sort
        val sortParams = params map { case Symbol(p) => SortParam(p) }
        val sort = PSortExpr(SortSym(name), sortParams)
        // Analyse cases
        for (c <- cases)
          c match {
            case Symbol(name) => {
              val const = FunSym(name)
              // A 0-ary constructor
              if ((declaredCFuns contains const) || (declaredPFuns contains const) || (definedFuns contains const))
                throw Error("constructor already declared or defined: " + name)
              // println("xxx constructor" + (const -> ((sortParams, List.empty, sort))))
              declaredPFuns += (const -> ((sortParams, List.empty, sort)))
              declaredPBoolFuns += (FunSym(PBParser.indexedName("is", Seq(name))) -> ((sortParams, List(sort))))
            }
            case SExprList(Symbol(name) :: dests) => {
              val const = FunSym(name)
              if ((declaredCFuns contains const) || (declaredPFuns contains const) || (definedFuns contains const))
                throw Error("constructor already declared or defined: " + name)
              // const is the constructor, 
              // dests the definitions destructors
              // We go through the destructors one by one and declare them.
              // We also remember their sorts, which determines the sorts of the 
              // constructor
              var constParamSorts = List.empty[PSort]
              for (d <- dests)
                d match {
                  case Pair(Symbol(name), destSortExpr) => {
                    val dest = FunSym(name)
                    if ((declaredCFuns contains dest) || (declaredPFuns contains dest) || (definedFuns contains dest))
                      throw Error("destructor already declared or defined: " + name)
                    val realDestSort =
                      if (destSortExpr == sortSymbol)
                        // Use the sort expression instead of the sort name
                        sort
                      else
                        parsePSort(destSortExpr, params.asInstanceOf[List[Symbol]]) // use what is given
                    constParamSorts ::= realDestSort
                    // Declare the destructor:
                    declaredPFuns += (dest -> ((sortParams, List(sort), realDestSort)))
                  }
                  case _ => throw Error("ill-formed destructor definition: " + d)
                }
              // Declare the constructor:
              constParamSorts = constParamSorts.reverse
              // println("xxx constructor" + (const -> ((sortParams, constParamSorts, sort))))
              declaredPFuns += (const -> ((sortParams, constParamSorts, sort)))
              declaredPBoolFuns += (FunSym(PBParser.indexedName("is", Seq(name))) -> ((sortParams, List(sort))))
            }
            case _ => Error("ill-formed constructor definition: " + c)
          }
        // finally generate the declarations and assertions to define this datatype
        datatypeToTemplates(params, sortSymbol, cases)
      }

      // declare-sort
      case SExprList(List(Symbol("declare-sort"), Symbol(name), Numeral(arity))) => {
        val sortSym = SortSym(name)
        if ((declaredSorts contains sortSym) || (definedSorts contains sortSym))
          throw Error("sort already declared or defined: " + name)
        declaredSorts += (sortSym -> arity.toInt)
      }

      // define-sort
      case SExprList(List(Symbol("define-sort"), Symbol(name), SExprList(params), body)) => {
        val sortSym = SortSym(name)
        if (!(isSymbols(params) && allDistinct(params)))
          throw Error("non-symbol or duplicate symbol in parameter list of sort definition: " + cmd)
        if ((declaredSorts contains sortSym) || (definedSorts contains sortSym))
          throw Error("sort already declared or defined: " + name)
        definedSorts += (sortSym -> (
          (params map { case Symbol(p) => SortParam(p) }),
          parsePSort(body, params.asInstanceOf[List[Symbol]])))
      }

      // declare-const
      case SExprList(List(Symbol("declare-const"), Symbol(name), resSort)) => {
        val f = FunSym(name)
        if (symbolIsDefined(f))
          throw Error("constant already declared or defined: " + name)
        declaredCFuns += (f -> (List.empty, parseCSort(resSort)))
      }

      // declare-fun
      case SExprList(List(Symbol("declare-fun"), Symbol(name), SExprList(argsSorts), resSort)) => {
        val f = FunSym(name)
        // if ((declaredCFuns contains f) || (declaredPFuns contains f) || (definedFuns contains f))
        if ((declaredCFuns contains f) || (definedFuns contains f))
          throw Error("function already declared or defined: " + name)
        declaredCFuns += (f -> (argsSorts map { parseCSort(_) }, parseCSort(resSort)))

      }

      // declare-parametric-fun
      case SExprList(List(Symbol("declare-parametric-fun"), SExprList(params), Symbol(name), SExprList(argsSorts), resSort)) => {
        val f = FunSym(name)
        if (!(isSymbols(params) && allDistinct(params)))
          throw Error("non-symbol or duplicate parameter symbol parametric function declaration: " + cmd)
        if ((declaredCFuns contains f) || (declaredPFuns contains f) || (definedFuns contains f))
          throw Error("function already declared or defined: " + name)
        val sparams = params.asInstanceOf[List[Symbol]]
        val sortParams = params map { case Symbol(p) => SortParam(p) }
        declaredPFuns += (f -> (sortParams, argsSorts map { parsePSort(_, sparams) }, parsePSort(resSort, sparams)))
      }

      // define-const
      case defi @ SExprList(List(Symbol("define-const"), Symbol(name), resSort, body)) => {
        val f = FunSym(name)
        if ((declaredCFuns contains f) || (declaredPFuns contains f) || (definedFuns contains f))
          throw Error("constant already declared or defined: " + name)
        declaredCFuns += (f -> (List.empty, parseCSort(resSort)))
        definedFuns += (f -> ((Map.empty[Var, CSort],
          parseCSort(resSort),
          parseTerm(body, Map.empty),
          defi.toString)))
      }

        // define-fun
        // note that in the latest SMT-Lib spec. this requires that the defining term does not contain f
      case defi @ SExprList(List(Symbol("define-fun"), Symbol(name), SExprList(sortedVars), resSort, body)) => {
        val f = FunSym(name)
        if (symbolIsDefined(f))
          throw Error("constant already declared or defined: " + name)
        if (!variablesOK(sortedVars))
          throw Error("ill-formed formal parameter list: " + cmd)

        //new guard - how to find the operators of a Term?

        // These two are used in the Forall/Exists constructor:
        val qVars = Map.empty[Var, CSort] ++ toVarSort(sortedVars)
        val t: Term = parseTerm(body, qVars)
        if (t.symbols(f))
          throw Error("illegal use of recursion in define-fun: " + cmd)
        definedFuns += (f -> ((qVars,
          parseCSort(resSort),
          t,
          //parseTerm(body, (sortedVars map { case Pair((v @ Symbol(_)), _) => v }).toSet),
          defi.toString)))
      }
        // define-fun-rec: alias for single application of define-funs-rec
      case SExprList(List(Symbol("define-fun-rec"), Symbol(name), SExprList(sortedVars), resSort, body)) => {
        val f = FunSym(name)
        if (symbolIsDefined(f))
          throw Error("constant already declared or defined: " + name)
        if (!variablesOK(sortedVars))
          throw Error("ill-formed formal parameter list: " + cmd)

        val qVars = Map.empty[Var, CSort] ++ toVarSort(sortedVars)
        definedFuns += (f -> ((qVars,
          parseCSort(resSort),
          parseTerm(body, qVars),
          cmd.toString)))
      }
        // define-funs-rec: (define-funs-rec (f1 sig) (f2 sig) t1 t2)
      case SExprList(List(Symbol("define-funs-rec"), SExprList(funSigs), SExprList(funBodies))) => {
        (funSigs zip funBodies) foreach { case (SExprList(List(Symbol(name), SExprList(sortedVars), resSort)), body) =>
          val f = FunSym(name)
          if (symbolIsDefined(f))
            throw Error("constant already declared or defined: " + name)
          if (!variablesOK(sortedVars))
            throw Error("ill-formed formal parameter list: " + cmd)
          val qVarsVars = sortedVars map { case Pair(Symbol(name), _) => Var(name, 0) }
          val qVarsSorts = sortedVars map { case Pair(_, s) => parseCSort(s) }
          val qVarSort = Map.empty[Var, CSort] ++ (qVarsVars zip qVarsSorts)
          definedFuns += (f -> (qVarSort,
            parseCSort(resSort),
            parseTerm(body, qVarSort), 
            cmd.toString))
        }
      }
      case SExprList(List(Symbol("echo"), StringSExpr(s))) => smttotptp.message(s) // Now, that was easy! 

      // assert 
      case SExprList(List(Symbol("assert"), s)) => {
        val f = parseTerm(s, Map.empty)
        if (f.xsort(Map.empty) != BoolSort)
          throw Error("asserted term is not of type Bool: " + cmd)
        assertions += ((f, f))
      }

/*      case SExprList(List(Symbol(n))) if Set("check-sat", "get-assertions", "push", "pop", "get-assignment",
        "get-proof", "get-unsat-core") contains n =>
        smttotptp.warning("ignoring command " + cmd)
      case SExprList(List(Symbol(n), _)) if Set("set-info", "get-value", "get-option", "get-info") contains n =>
        smttotptp.warning("ignoring command " + cmd)
      case _ => throw Error("command ill-formed or not (yet?) handled: " + cmd)
 */
      case SExprList(List(Symbol("push"))) => throw Error("unsupported command: push")
      case SExprList(List(Symbol("pop"))) => throw Error("unsupported command: pop")
      case _ => smttotptp.warning("ignoring command " + cmd)
    }
  }

  def parseCommands(cmds: List[SExpr]) {
    import scala.util.control._
    val mybreaks = new Breaks
    import mybreaks.{ break, breakable }
    breakable {
      for (cmd <- cmds) {
        if (cmd == FunApp0("exit"))
          break()
        parseCommand(cmd)
      }
    }
  }

  def addListDatatype() {
    if (!listDataTypeAdded) {
      parseCommand(PBParser.parseSExpr("(declare-z3-datatype (T) ((List nil (insert (head T) (tail (List T))))))"))
      listDataTypeAdded = true
    }
  }

  def addArrayDeclarations() {
    val decls = """
   (declare-sort Array 2)
   (declare-parametric-fun (I E) select ((Array I E) I) E)
   (declare-parametric-fun (I E) store ((Array I E) I E) (Array I E))
   (declare-parametric-fun (I E) const (E) (Array I E))
"""

    if (!arrayDeclarationsAdded) {
      parseCommands(PBParser.parseSExprs(decls))
      arrayDeclarationsAdded = true
    }
  }

  /** Stores the RHS (i.e. source) of assertions arising from definitions, for later identification */
  var defineAssertions = Set[Term]()

  /*
   * Expand all defined functions into assertions
   */
  def expandDefinedFunctions() {
    while (!definedFuns.isEmpty) {
      val (fun, (varSort, resSort, body, src)) = definedFuns.head
      definedFuns = definedFuns.tail
      val (vars, sorts) = varSort.toList.unzip
      declaredCFuns += (fun -> (sorts, resSort))
      val f = 
        if (varSort.isEmpty)
          App(FunSym("="), List(App(fun, List.empty), body)) 
        else
          Forall(varSort.toList, App(FunSym("="), List(App(fun, vars), body)))
      defineAssertions += f
      assertions += ((f,f)) // appends
    }
  }

  /*
   * Return all (concrete) sorts of all subterms in the given list of (closed) terms
   * Used for addAxioms
   */
  def allSorts(ts: Iterable[Term]) = {

    var res = Set.empty[CSort]

    // add the sorts of all subterms of t to res
    def addSort(t: Term, varSort: Map[Var, CSort]) {
      res += t.xsort(varSort)
      t match {
        case App(_, args) => args foreach { addSort(_, varSort) }
        case Let(bindings, body) => {
          // need to collect the sorts in the bound terms 
          bindings.cod foreach { addSort(_, varSort) }
          // recurse into the body
          val letVarSort = bindingsToVarSort(bindings, varSort)
          addSort(body, varSort ++ letVarSort)
        }
        case Exists(vars, body) => { 
          addSort(body, varSort ++ vars)
          addSortVars(vars, varSort ++ vars)
        }
        case Forall(vars, body) => { 
          addSort(body, varSort ++ vars)
          addSortVars(vars, varSort ++ vars)
        }
        case _ => () // Const and Var, have collected the sort already above
      }
    }

    def addSortVars(vars: List[(Var, CSort)], varSort: Map[Var, CSort]) {
      vars foreach { vt => addSort(vt._1, varSort) }
    }

    ts foreach { addSort(_, Map.empty) }
    res
  }

  def addArrayAxioms() {

    // generate the array axioms for two given sorts and assert them
    def addArrayAxiomsInner(s1: CSort, s2: CSort) {
      val s1t = s1.forTemplate
      val s2t = s2.forTemplate
      val axioms = List(
        // The first two axioms are not redeclarations of their corresponding *parametric* declarations
        // "(declare-fun select ((Array %s %s) %s) %s)".format(s1, s2, s1, s2),
        // "(declare-fun store ((Array %s %s) %s %s) (Array %s %s))".format(s1, s2, s1, s2, s1, s2),
        "(assert (forall ((a (Array %s %s)) (i %s) (e %s)) (= (select (store a i e) i) e)))".format(s1t, s2t, s1t, s2t),
        "(assert (forall ((a (Array %s %s)) (i %s) (j %s) (e %s)) (=> (distinct i j) (= (select (store a i e) j) (select a j)))))".format(s1t, s2t, s1t, s1t, s2t),
        "(assert (forall ((a (Array %s %s)) (b (Array %s %s))) (=> (forall ((i %s)) (= (select a i) (select b i))) (= a b))))".format(s1t, s2t, s1t, s2t, s1t),
        "(assert (forall ((i %s) (e %s)) (= (select ((as const (Array %s %s)) e) i) e)))".format(s1t, s2t, s1t, s2t))
      for (a <- axioms) {
        parseCommand(PBParser.parseSExpr(a))
      }
    }

    assume(definedFuns.isEmpty) // because we look into assertions only

    for (s <- allSorts(assertions map { _._1 }))
      s match {
        case CSort(SortSym("Array"), List(iSort, eSort)) =>
          addArrayAxiomsInner(iSort, eSort)
        case _ => ()
      }
  }

  def addDatatypeAxioms() {

    def addDatatypeAxiomsInner(templates: List[Template], sorts: List[CSort]) {
      val sortsAsStrings = sorts map { _.forTemplate }
      for (t <- templates)
        parseCommand(PBParser.parseSExpr(t(sortsAsStrings)))
    }

    assume(definedFuns.isEmpty) // because we look into assertions only

    for (s <- allSorts(assertions map { _._1 }))
      s match {
        case CSort(sortSym, sorts) =>
          datatypeDefs.get(sortSym) match {
            case None => ()
            case Some(templates) =>
              addDatatypeAxiomsInner(templates, sorts)
          }
        case _ => ()
      }
  }

  /*
   *  Expand all let-binders in all assertions, preserve attributes
   */
  def expandLet() {
    assertions = assertions map { case (f, src) => (f.letExpanded(Map.empty), src) }
  }

  /*
   *  Expand all ite expressions in all assertions
   */
  def expandIte() {
    assertions = assertions map { case (f, src) => (f.iteExpanded(Map.empty), src) }
  }

  /*
   * Expand all quantifications over boolean variables.
   * See README.md for an example why this might be useful
   */
  def expandBooleanQuantification() {
    assertions = assertions map { case (f, src) => (f.boolQuantElim, src) }
  }

  def simplifyAssertions() {
    assertions = assertions map { case (f, src) => (f.simplify, src) }
  }

  /*
   *  Eliminate all term-level Booleans (term-level in the sense of TPTP)
   */
 def elimBool() {
   // Works in two phases:
   // - first introduce names for "complex" boolean formulas in term-level positions.
   //   This requires a pseudo-boolean sort on the term level.
   //   Add axioms for the pseudo-boolean sort.
   // - second change the signature of all function symbols by replacing the Bool sort by
   //   the pseudo-boolean sort where appropriate.

   assume(definedFuns.isEmpty)

   // The operators on formula-level:
   val boolOpsNames = Set("true", "false", "not", "=>", "implies", "and", "or", "xor")

    // Construct the new assertions by inspecting and transforming the given assertions,
    // one by one. This may add to auxiliary definitions which in turn need to be inspected.
    // Does not introduce the term-level pseudo-bool sort. 

   var open = assertions // List of assertions still to be done
    assertions = ListBuffer.empty[(Term, Term)] // List of assertions already done

   var needTLBool = false // Whether the term-vel bool sort is needed

    def define(t: Term, varSort: Map[Var, CSort]) = {
      // t is a Bool-sorted term. It is replaced by a definition unless
      // it is a variable or a constant
      t match {
        case c: Const => c // Boolean constant doesn't exist anyway
        case v: Var => v
        case TrueTerm => TrueTerm
        case FalseTerm => FalseTerm
        case t => {
          val defFunSym = Term.genFunSym("def", true)
          val tfvars = t.fvars
          // The free variables in t along with their sorts, all of which must be contained in varSort
          val tVarSort = varSort filter { tfvars contains _._1 }
          // Add a declaration for defFunSym
          declaredCFuns += (defFunSym -> (tVarSort.values.toList, BoolSort))
          // Make the definition term
          val defTerm = App(defFunSym, tVarSort.keys.toList)
          // Add a definition
          val e = App(FunSym("="), List(defTerm, t))
          open += ((
            if (tVarSort.isEmpty) e else Forall(tVarSort.toList, e),
            t))
          defTerm
        }
      }
    }

    def liftBool(t: Term, varSort: Map[Var, CSort], inTerm: Boolean): Term = {
      // println(s"liftBool($t, $inTerm)")
      if (inTerm && (t.xsort(varSort) == BoolSort)) {
        needTLBool = true
        define(t, varSort)
      }
      else {
        // In a formula context or t is a non-Bool sorted term
        t match {
          case c: Const => c // Numeral, String
          case v: Var => v // if (inTerm) v else App(FunSym("="), List(v, TrueTerm))
          case App(As(fun, sort), args) =>
            // Is this the right thing?
            App(As(fun, sort), args map { liftBool(_, varSort, inTerm) })
          case App(fun @ FunSym(op), args) => {
            if ((boolOpsNames contains op)  ||
              (op == "=" && (args(0).xsort(varSort) == BoolSort))) { // "=" is "<=>"
            // Must be in a formula context
               // Can directly translate with this op into formula
              App(fun, args map { liftBool(_, varSort, false) })
            } else {
              // t is a non-Bool sorted term or is an ordinary Bool-sorted atom
              App(fun, args map { liftBool(_, varSort, true) })
            }
          }
          case Exists(vars, body) => {
            // Must be in a formula context, otherwise would have bee defineed
            if (vars exists { _._2 == BoolSort }) needTLBool = true
            Exists(vars, liftBool(body, varSort ++ vars, false))
          }
          case Forall(vars, body) => {
            if (vars exists { _._2 == BoolSort }) needTLBool = true
            Forall(vars, liftBool(body, varSort ++ vars, false))
          }
          case Let(bindings, body) =>
            throw Error("Bool-sorted let-terms require '(set-option :expand-let true)'")
        }
      }
    }

    // Loop over open and eliminate Bool-sorted terms by introduction of definitions
    // This keeps the sort Bool in place
    while (open.nonEmpty) {
      val (next, src) = open.head
      open = open.tail
      val h = liftBool(next, Map.empty, false)
      // println(s"liftBool($next) = $h")
      assertions += ((h.withAttributes(next.attributes), src))
    }

    // In a second pass replace all the occurrences of the Bool sort of terms (in the TPTP sense)
    // by the term-level bool sort and correspondingly re-sort the Bool-sorted terms.
    // In order not to confuse xsort we do this on a copy.

    def reSortSort(sort: CSort) = if (sort == BoolSort) TLBoolSort else sort

    def reSortDecl(fun: FunSym, decl: (List[CSort], CSort)) = {
      ((fun -> ((decl._1 map { reSortSort(_) }, if (fun.tlBoolSorted) TLBoolSort else decl._2))))
    }

    def reSortVars(vars: List[(Var, CSort)]) = vars map { case(v, s) => ((v, reSortSort(s))) }

    def reSortTerm(t: Term, varSort: Map[Var, CSort], inTerm: Boolean): Term = {
      // println(s"reSortTerm($t, $inTerm)")
      if (inTerm && (t.xsort(varSort) == BoolSort))
        t match {
          case c: Const => c // Numerical and string literals, Boolean constant doesn't exist anyway
          case v: Var => v // Variable will be re-sorted to term-level Bool
          case TrueTerm => TLTrueTerm
          case FalseTerm => TLFalseTerm
          // Otherwise t must be a definition
          case t => t
        } else 
          // In a formula context or t is non-Bool sorted term
          t match {
            case c: Const => c // Numeral, String
            case v: Var => if (inTerm) v else App(FunSym("="), List(v, TLTrueTerm))
            case App(As(fun, sort), args) =>
              // Is this the right thing?
              App(As(fun, sort), args map { reSortTerm(_, varSort, inTerm) })
            case App(fun @ FunSym(op), args) => {
              if ((boolOpsNames contains op)  ||
                (op == "=" && (args(0).xsort(varSort) == BoolSort))) { // "=" is "<=>"
                // Must be in a formula context, otherwise would have been extracted above
                // Can directly translate with this op into formula
                App(fun, args map { reSortTerm(_, varSort, false) })
              } else if (fun.tlBoolSorted) {
                // t is an atom made from a definition
                App(FunSym("="), List(App(fun, args map { reSortTerm(_, varSort, true) }), TLTrueTerm))
              } else {
                // t is a non-Bool sorted term or is an ordinary Bool-sorted atom
                App(fun, args map { reSortTerm(_, varSort, true) })
              }
            }
            case Exists(vars, body) =>
              // Must be in a formula context, otherwise would have bee defineed
              Exists(reSortVars(vars), reSortTerm(body, varSort ++ vars, false))
            case Forall(vars, body) =>
              Forall(reSortVars(vars), reSortTerm(body, varSort ++ vars, false))
            case Let(bindings, body) =>
              throw Error("Bool-sorted let-terms require '(set-option :expand-let true)'")
          }
    }

   if (needTLBool) { 
     val declaredCFunsNew = declaredCFuns map { case (fun, decl) => reSortDecl(fun, decl) }
     val assertionsNew = assertions map { case (t, src) => 
         val atts = t.attributes
         ((reSortTerm(t, Map.empty, false).withAttributes(atts),src)) }

     declaredCFuns = declaredCFunsNew
     assertions = assertionsNew

     // Introduce the term-level constants true and false along with their sort and axioms.
     declaredSorts += (TLBoolSym -> 0)
     declaredCFuns += (TLTrue -> (List.empty, TLBoolSort))
     declaredCFuns += (TLFalse -> (List.empty, TLBoolSort))
     List(
       "(assert (forall ((b tlbool)) (or (= b tltrue) (= b tlfalse))))",
       "(assert (not (= tltrue tlfalse)))"
     ) foreach { s =>
       parseCommand(PBParser.parseSExpr(s))
     }
   }
 }

  def show() {
    println()
    println("Declared sorts:\n  " +
      (declaredSorts map { sa => sa._1 + ":" + sa._2 }).toList.toMyString("", ", ", ""))
    println()
    println("Defined sorts:")
    for ((sort, (params, resSort)) <- definedSorts)
      println("  " + sort + params.toMyString("", "[", ", ", "]") + " = " + resSort)
    println()
    println("Declared functions:")
    for ((fun, (argsSorts, resSort)) <- declaredCFuns)
      println("  " + fun + ": " + argsSorts.toMyString("", " × ", "") +
        (if (argsSorts.isEmpty) "" else " ↦ ") + resSort)
    println()
    println("Declared parametric functions:")
    for ((fun, (sortParams, argsSorts, resSort)) <- declaredPFuns)
      println("  " + fun + sortParams.toMyString("", "[", ", ", "]") + ": " + argsSorts.toMyString("", " × ", "") +
        (if (argsSorts.isEmpty) "" else " ↦ ") + resSort)
    println()
    println("Declared parametric boolean:")
    for ((fun, (sortParams, argsSorts)) <- declaredPBoolFuns)
      println("  " + fun + sortParams.toMyString("", "[", ", ", "]") + ": " + argsSorts.toMyString("", " × ", "") +
        (if (argsSorts.isEmpty) "" else " ↦ ") + BoolSort)
    println()
    println("Defined Functions:")
    for ((fun, (varSort, resSort, body, _)) <- definedFuns)
      println("  " + fun + (varSort map { vs => vs._1 + ":" + vs._2 }).toList.toMyString("", "(", ", ", ")") +
        ":" + resSort + " = " + body)
    println()
    println("Assertions:")
    for (f <- assertions)
      println(f._1.toStringPP(0))
    println()

  }
}
