package smttotptp

import org.parboiled2._
//import util._

class PBParser(val input: ParserInput) extends Parser {

  val otherSymb = CharPredicate("-+<>/=_!$@~%&*.?^")
  val whiteSpaceChar = CharPredicate(" \t\r\f\n")

  def ws = rule { whiteSpaceChar.* }

  def >>(c: Char): Rule0 = rule { ch(c) ~ ws }
  def <<(c: Char): Rule0 = rule { ws ~ ch(c) }

  /** Expect complete lines of input, possibly empty */
  def SMTLibInput = rule { ws ~ ( pSExprList | pComment ).*(whiteSpaceChar.+) ~ ws ~ EOI }

  def pSExpr: Rule1[SExpr] = rule {
    pComment | pSpecConstant | pSymbol | pKeyword | pSExprList
  }

  def pSpecConstant = rule {
    pDecimal | pNumeral | pString
  }

  def pSExprList: Rule1[SExpr] = rule {
    >>('(') ~ zeroOrMore(pSExpr).separatedBy(oneOrMore(whiteSpaceChar)) ~ <<(')') ~> ((sexprs: Seq[SExpr]) => SExprList(sexprs.toList))
  }

  def pAttributeValue = rule {
    pSpecConstant | pSymbol | pSExprList
  }

  def pAttribute = rule {
    pKeyword ~ optional(pAttributeValue)
  }

  def pNumeral: Rule1[Numeral] = rule {
    ( capture(CharPredicate.Digit19 ~ CharPredicate.Digit.*) ~> ((s:String) => Numeral(BigInt(s)))) | 
    (ch('0') ~> (() => Numeral(0)))
  }

  def pDecimal = rule {
    capture(CharPredicate.Digit.+) ~ "." ~ capture(CharPredicate.Digit.+) ~> {
      (n: String, d: String) => Decimal(Rat((n + d).toInt, ("1" + "0"*d.length).toInt))
    }
  }

  def pString = rule {
   '"' ~ capture(noneOf("\"").*) ~ '"' ~> StringSExpr
  }

  def index = rule {
      simpleSymbol |
      (pNumeral ~> { _.n.toString })
    }

  def indexedSymbol = rule {
      >>('(') ~ >>('_') ~ simpleSymbol ~ ws ~ oneOrMore(index) ~ <<(')') ~> { (s, idxs) => Symbol(PBParser.indexedName(s, idxs)) }
    }

  def pSymbol = rule {
     ( simpleSymbol ~> Symbol ) |
     indexedSymbol |
     barredSymbol
  }

  def barredSymbol = rule {
    '|' ~ capture(noneOf("|\\").*) ~ '|' ~> Symbol
  }

  def pKeyword = rule {
    ':' ~ simpleSymbol ~> ((s:String) => Keyword(":" + s))
  }

  def simpleSymbol = rule {
     capture((CharPredicate.Alpha | otherSymb) ~ (CharPredicate.AlphaNum | otherSymb).*)
  }

  def pComment = rule {
    ';' ~ capture(noneOf("\n").*) ~> (x => FunApp1("__comment__", StringSExpr(x)))
  }

}

object PBParser {
  import scala.util.{Try, Failure, Success}

  def indexedName(s: String, idxs: Iterable[String]) = s + "-" + idxs.mkString("-")

  private def isComment(sexpr: SExpr) = 
    sexpr match { case FunApp1("__comment__", _) => true; case _ => false }

  /** @throws FileNotFoundException */
  def parseSMTLibFile(filename: String): Seq[SExpr] = {

    val in: Iterator[String] = io.Source.fromFile(filename).getLines()
    var res = Vector[SExpr]()

    //drop empty lines in prefix
    in.dropWhile(_.isEmpty)

    while(in.hasNext) {
      var openBrackets = 0
      var chunk = List[String]()

      val li = in.next
      chunk ::= li

      li foreach {
        case '(' => openBrackets += 1
        case ')' => openBrackets -= 1
        case _ => ()
      }

      //pop lines while open is nonzero
      if (openBrackets != 0) {
        while (openBrackets != 0 && in.hasNext) {
          val li2 = in.next
          li2 foreach {
            case '(' => openBrackets += 1
            case ')' => openBrackets -= 1
            case _ => ()
          }
          chunk ::= li2
        }
      }

      val parser = new PBParser(chunk.reverse.mkString(" "))

      parser.SMTLibInput.run() match {
        case Success(s: Seq[SExpr]) => 
          res ++= (s filterNot { isComment _ })
        case Failure(f: ParseError) => {
          println(parser.formatError(f))
          System.exit(1)
        }
        case Failure(t) =>
          throw t //propagate other exceptions
      }
    }
    res
  }

  def parseSExpr(in: String): SExpr = 
    parseSExprs(in).head

  def parseSExprs(in: String): List[SExpr] = {
    val p = (new PBParser(in))
    p.SMTLibInput.run() match {
      case Success(s) => s.toList
      case Failure(f: ParseError) => 
        throw new Exception("Invalid input to interal method parseSExpr:\n"+p.formatError(f))
      case Failure(t) =>
        throw t
    }
  }

}
