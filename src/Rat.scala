package smttotptp

/*
 * Rational Numbers data structure
 */

class Rat(top: BigInt, bot: BigInt) extends Ordered[Rat] {
  require(bot != 0)

  // def this(n: Int) = this(n, 1)

  private def gcd(a: BigInt, b: BigInt): BigInt =
    if (b == 0) a else gcd(b, a % b)

  private val g = gcd(top.abs, bot.abs)
  
  // numer (i.e nominator) and denom are the publicly visible components
  // Make sure denom is always > 0: 
  val (numer, denom) = if (bot < 0) (-top / g, -bot / g) else (top / g, bot / g)

  def compare(r: Rat): Int = {
    if (numer * r.denom < r.numer * denom) -1
    else if (numer * r.denom > r.numer * denom) 1
    else 0
  }

  override def equals(that: Any) =
    that match {
      case r: Rat ⇒ numer == r.numer && denom == r.denom
      case _      ⇒ false
    }

  def signum = numer.signum
  
  def isRatInt = denom == 1

  /** Whether this is castable to an Int */
  def isValidInt = isRatInt && numer.isValidInt

  // Operations and comparisons on Rational
  def *(r: Rat) = new Rat(numer * r.numer, denom * r.denom)
  def /(r: Rat) = new Rat(numer * r.denom, denom * r.numer)
  def +(r: Rat) = new Rat(numer * r.denom + r.numer * denom, denom * r.denom)
  def -(r: Rat) = new Rat(numer * r.denom - r.numer * denom, denom * r.denom)
  def uminus = new Rat(-numer, denom)
  def abs = new Rat(numer.abs, denom.abs)
  def sign = if (numer < 0) -1 else 1 // recall denom is always > 0

  // Int operations and comparisons
  def /(i: BigInt) = this * new Rat(1, i)
  def *(i: BigInt) = new Rat(numer * i, denom)
  def ==(i: BigInt) = numer == i && denom == 1 // todo: should use equals above?
  def !=(i: BigInt) = !(this == i)
  def <(i: BigInt) = numer < i * denom
  def >(i: BigInt) = numer > i * denom
  def <=(i: BigInt) = this < i || this == i
  def >=(i: BigInt) = this > i || this == i

  override def toString =
    if (numer == 0)
      "0"
    else if (denom == 1)
      numer.toString
    else
      numer + "/" + denom
      
  def toDouble = (numer / denom).toDouble
}

object Rat {
  def apply(top: BigInt, bot: BigInt) = new Rat(top, bot)
  def unapply(that: Any) = that match {
    case r: Rat ⇒ Some((r.numer, r.denom))
    case _      ⇒ None
  }
}

object RatInt {
  def apply(x: BigInt) = new Rat(x, 1)
  def unapply(that: Any) = that match {
    case x: Rat if x.isRatInt ⇒ Some(x.numer)
    case _                    ⇒ None
  }
}

object RatImplicits {
  implicit def Int2Rat(i: Int) = Rat(i,1)
  implicit def BigInt2Rat(i: BigInt) = Rat(i,1)
}
