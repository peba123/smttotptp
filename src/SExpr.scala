package smttotptp

import util._

abstract class SExpr

abstract class SpecConstant extends SExpr
case class Numeral(n: BigInt) extends SpecConstant {
  assert(n >= 0)
  override def toString = n.toString
}
case class Decimal(d: Rat) extends SpecConstant {
  override def toString = d.toDouble.toString
}
case class StringSExpr(s: String) extends SpecConstant {
  override def toString = s
}

case class Symbol(s: String) extends SExpr {
  override def toString = s
}
case class Keyword(s: String) extends SExpr {
  require(s(0)==':', s"Keyword $s must be prefixed with ':'")
  override def toString = s
}
case class SExprList(sexprs: List[SExpr]) extends SExpr {
  override def toString = 
    this match {
      case SExprList(List(Symbol("__comment__"), StringSExpr(x))) => x
      case _ => 
        if (sexprs.isEmpty) "()" else
          "(" + (sexprs map { _.toString } reduceLeft { _ + " " + _ }) + ")"
    }
}

// handy views on SExprs

object Pair {
  def apply(s1: SExpr, s2: SExpr) = SExprList(List(s1, s2))
  def unapply(s: SExpr) = s match {
    case SExprList(List(s1, s2)) => Some((s1, s2))
    case _ => None
  }
}

object FunApp {
  def apply(op: String, args: List[SExpr]) = SExprList(Symbol(op) :: args)
  def unapply(s: SExpr) = s match {
    case SExprList(Symbol(op) :: args) => Some((op, args))
    case _ => None
  }
}

object FunApp0 {
  def apply(op: String) = SExprList(List(Symbol(op)))
  def unapply(s: SExpr) = s match {
    case SExprList(List(Symbol(op))) => Some(op)
    case _ => None
  }
}

object FunApp1 {
  def apply(op: String, arg: SExpr) = SExprList(List(Symbol(op), arg))
  def unapply(s: SExpr) = s match {
    case SExprList(List(Symbol(op), arg)) => Some((op, arg))
    case _ => None
  }
}

object FunApp2 {
  def apply(op: String, arg1: SExpr, arg2: SExpr) = SExprList(List(Symbol(op), arg1, arg2))
  def unapply(s: SExpr) = s match {
    case SExprList(List(Symbol(op), arg1, arg2)) => Some((op, arg1, arg2))
    case _ => None
  }
}

object FunApp3 {
  def apply(op: String, arg1: SExpr, arg2: SExpr, arg3: SExpr) = SExprList(List(Symbol(op), arg1, arg2, arg3))
  def unapply(s: SExpr) = s match {
    case SExprList(List(Symbol(op), arg1, arg2, arg3)) => Some((op, arg1, arg2, arg3))
    case _ => None
  }
}

object FunApp4 {
  def apply(op: String, arg1: SExpr, arg2: SExpr, arg3: SExpr, arg4: SExpr) = 
    SExprList(List(Symbol(op), arg1, arg2, arg3, arg4))
  def unapply(s: SExpr) = s match {
    case SExprList(List(Symbol(op), arg1, arg2, arg3, arg4)) => Some((op, arg1, arg2, arg3, arg4))
    case _ => None
  }
}

/*
object ITE {
  def apply(cond: SExpr, thent: SExpr, elset: SExpr) = 
    FunApp3("ite", cond, thent, elset)
  def unapply(s: SExpr) = s match {
    case FunApp3("ite", cond, thent, elset) => Some((cond, thent, elset))
    case _ => None
  }
}
 */



