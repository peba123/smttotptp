package smttotptp

import util._

class TPTPPrinter(out: StringListBuffer) {

  case class Error(s: String) extends Exception

  // not needed - have unquoted already in the SExpr parser
  // def unQuoteSMT(s: String) = if (s.head == '|' && s.last == '|') s.drop(1).dropRight(1) else s

  def isLowerAlpha(c: Char) = 'a' <= c && c <= 'z'
  def isUpperAlpha(c: Char) = 'A' <= c && c <= 'Z'
  def isNumeric(c: Char) = '0' <= c && c <= '9'
  def isAlphaNumeric(c: Char) = isLowerAlpha(c) || isUpperAlpha(c) || isNumeric(c) || c == '_'
  def isLowerWord(s: String) = isLowerAlpha(s.head) && (s.tail forall { isAlphaNumeric(_) })

  def toAlphaNumericIfPossible(s: String) =
    // This may collapse different identifiers, but it is probably safe ...
    s.replaceAll("-", "_").replaceAll("'", "prime")

  def toAtomicWord(s: String) = {
    assume(!s.isEmpty)
    val hs = toAlphaNumericIfPossible(s)
    if (isLowerWord(hs))
      hs
    else
      // give up and quote 
      "'" + hs + "'"
  }

  def isTPTPVar(s: String) =
    isUpperAlpha(s.head) && (s.tail forall { isAlphaNumeric(_) })

  val varCtr = new Counter()
  def newVar(s: String) = "%s__%d__".format(s, varCtr.next())

  def canonical(s: String) = {
    // Replace non-alphanumeric characters by '_'
    import collection.mutable.ListBuffer

    val m = Map(
      '~' -> "_tilde_",
        '!' -> "_bang_",
        '@' -> "_at_",
        '$' -> "_dollar_",
        '%' -> "_percent_",
        '^' -> "_caret_",
        '&' -> "_ampersand_",
        '*' -> "_star_",
        '-' -> "_minus_",
        '+' -> "_plus_",
        '=' -> "_equals_",
        '<' -> "_less_",
        '>' -> "_greater_",
        '.' -> "_dot_",
        '?' -> "_questionmark_",
        '/'  -> "_slash_",
        '''  -> "_prime_"
      )

    // Apply the above map to all characters in s
    var h1 = ""
    for (c <- s) {
      m.get(c) match {
        case None => h1 += c
        case Some(rep) => h1 ++= rep
      }
    }
    // val h1 = s map {
    //   _ match {
    //     case '~' | '!' | '@' | '$' | '%' | '^' | '&' | '*' | '-' | '+' | '=' | '<' | '>' | '.' | '?' | '/' => '_'
    //     case c => c
    //   }
    // }
    // See if h1 starts with an uppercase letter or can be converted into such
    if (isUpperAlpha(h1.head))
      h1
    else if (isLowerAlpha(h1.head))
      h1.head.toUpper + h1.tail
    else 
      // Something different - turn into a variable by prefixing with A_
      "A_" + h1
  }

  var specialVarMap = Map.empty[String, String]
  var reverseMap = Map.empty[String, String]

  def toTPTPVar(s: String) = {
    if (isTPTPVar(s))
      s 
    else {
      val sc = canonical(s)
      specialVarMap.get(sc) match {
        case Some(h) => h // readily use this
        case None => {
          // We see if we can use sc. This is possible if no  previous variable
          // different to s has the same canonical name
          reverseMap.get(sc) match {
            case None => {
              // Can use sc, but remember the pair sc -> c
              reverseMap += ((sc -> s))
              sc
            }
            case Some(t) if s == t => 
              // no conflict
              sc
            case Some(_) => {
              // conflicting previous string
              val res = newVar(sc)
              specialVarMap += ((s -> res))
              res
            }
          }
        }
      }
    }
  }


  def print(ast: AST) {
    import ast._

    def varToTPTP(v: Var) = toTPTPVar(v.toString)

/*    def varToTPTP(v: Var) = {
      val name = toAlphaNumericIfPossible(v.toString)
      if (name exists { !isAlphaNumeric(_) })
        throw this.Error("cannot convert into a TPTP variable: " + v)
      if (isUpperAlpha(name.head))
        name
      else if (isLowerAlpha(name.head))
        name.head.toUpper + name.tail
      else
        throw this.Error("cannot convert into a TPTP variable: " + v)
    }
 */ 
    // The concrete sorts used in all terms
    var sorts = Set.empty[CSort]

    def varWithSortToTPTP(vs: (Var, CSort)) = varToTPTP(vs._1) + ":" + sortToTPTP(vs._2)

    def sortToTPTP(sort: CSort, toAtomicWordFlag: Boolean = true): String = {
      val CSort(SortSym(name), args) = sort
      name match {
        case "Bool" if toAtomicWordFlag => "$o"
        case "Int" if toAtomicWordFlag => "$int"
        case "Real" if toAtomicWordFlag => "$real"
        case _ => {
          val s = name + (args map { sortToTPTP(_, false) }).toMyString("", "[", ",", "]")
          if (toAtomicWordFlag) toAtomicWord(s) else s
        }
      }
    }

    val tffPredefSorts = Set(IntSort, RealSort, BoolSort)
    val tffARIFuns = Map(
        "true" -> "$true",
        "false" -> "$false",
        "<" -> "$less",
        ">" -> "$greater",
        "<=" -> "$lesseq",
        ">=" -> "$greatereq",
        "+" -> "$sum",
        "-" -> "$difference",
        "*" -> "$product",
        "/" -> "$quotient")

    def funToTPTP(fun: FunSym, argsSorts: List[CSort], resSort: CSort) = {
      val FunSym(name) = fun
      
      // unary minus is a special case, as it's overloaded with subtraction
      if ((name == "-") && (argsSorts.length == 1))
        "$uminus"
      else tffARIFuns.get(name) match {
        case Some(s) => s
        case None => if ((declaredCFuns contains fun) || (definedFuns contains fun))
          // non-parametric case, don't need sort annotations to make the function name unique
          toAtomicWord(name)
        else
          toAtomicWord(
            name + ":" + (argsSorts map { sortToTPTP(_, false) }).toMyString("", "(", "*", ")") +
              (if (argsSorts.isEmpty) "" else ">") + sortToTPTP(resSort, false))
      }
    }

    def funRankToTPTP(funRank: (FunSym, List[CSort], CSort)) = {
      val (fun, argsSorts, resSort) = funRank
      funToTPTP(fun, argsSorts, resSort) + ": " +
        (if (argsSorts.isEmpty) ""
        else
          ((if (argsSorts.tail.isEmpty)
            sortToTPTP(argsSorts.head)
          else
            (argsSorts map { sortToTPTP(_) }).toMyString("", "(", " * ", ")")) + " > ")) + sortToTPTP(resSort)
    }

    var funRanks = Set.empty[(FunSym, List[CSort], CSort)]

    // FunSym("and") and FunSym("or") are excluded from leftAssocOps because we treat it specially,
    // see toTPTP
    val leftAssocOps = Set(FunSym("xor"), FunSym("+"), FunSym("-"), FunSym("*"), FunSym("/"), FunSym("div"))
    val rightAssocOps = Set(FunSym("=>"), FunSym("implies"))
    val chainableOps = Set(FunSym("="), FunSym("<"), FunSym(">"), FunSym("<="), FunSym(">="))
    val pairwiseOps = Set(FunSym("distinct"))

    def expandLeftAssoc(fun: FunSym, args: List[Term]) = args.reduceRight((s, t) => App(fun, List(s, t)))
    def expandRightAssoc(fun: FunSym, args: List[Term]) = args.reduceLeft((s, t) => App(fun, List(s, t)))
    def expandChainable(op: FunSym, args: List[Term]) =
      App(FunSym("and"),
        (for (
          ts <- args.tails;
          if (!ts.isEmpty) && (!ts.tail.isEmpty)
        ) yield App(op, List(ts.head, ts.tail.head))).toList)
    def expandPairwise(op: FunSym, args: List[Term]) =
      App(FunSym("and"),
        (for (
          ss <- args.tails;
          if (!ss.isEmpty);
          ts <- ss.tail.tails;
          if (!ts.isEmpty)
        ) yield App(op, List(ss.head, ts.head))).toList)

    def nlspaces(n: Int) =
      if (smttotptp.ppFlag)
        "\n" + ("\t" * (n / 8)) +  (" " * (n % 8))
      else "\n"

    // pretty print a list of terms with parenthesis and a separator
    def toTPTPList(ts: List[Term], varSort: Map[Var, CSort], left: String, sep: String, right: String, indent: Int): String = {
      left + " " +
        (for (tss <- ts.tails; if !tss.isEmpty) yield {
          toTPTP(tss.head, varSort, indent + left.length + 1) +
            // More formulas?
            (if (!tss.tail.isEmpty) {
              nlspaces(indent - (sep.length - 1)) +
                sep + " "
            } else
              "")
        }).mkString + ")"
    }

    /** returns the (first) ":named" assertion status attribute, also converts to TPTP name.
      *  E.g. :named hypothesis-123 becomes (Some(("hypothesis", "hypothesis_123")))
      */
    def getStatusAttribute(t: Term): Option[(String, String)] = {
      for (Attribute(k, v) <- t.attributes; if k == Keyword(":named")) {
        v match { 
          case Some(Symbol(value)) =>
            if (value startsWith "conjecture") return(Some(("conjecture", toAtomicWord(value))))
            else if (value startsWith "axiom") return(Some(("axiom", toAtomicWord(value))))
            else if (value startsWith "hypothesis") return(Some(("hypothesis", toAtomicWord(value))))
          case _ => ()
        }
      }
      return None
    }

    def isConjecture(t: Term) =
      getStatusAttribute(t) match {
        case Some(("conjecture", _)) => true
        case _ => false
      }

    def toTPTP(t: Term, varSort: Map[Var, CSort], indent: Int, topLevel: Boolean = false): String = {
      // topLevel tells us if toTPTP works on the top level of an assertion
      val statusAtt = getStatusAttribute(t)
      if (!topLevel && !(t.attributes.isEmpty || (statusAtt.nonEmpty && t.attributes.length != 1)))
        smttotptp.warning("ignoring attributes other than :named " + t.attributes.toMyString(" ", " ", " ") + "of term " + t)

      val resSort = t.xsort(varSort)
      sorts += resSort

      t match {
        case Const(const) =>
          const match {
            case Numeral(n) => n.toString
              // Bug fix: output decimals, not rationals
            // case Decimal(Rat(num, denom)) => num + "/" + denom
            case Decimal(r) => r.toDouble.toString
            case StringSExpr(s) => s
          }
        case v: Var => varToTPTP(v)

          // Special case: distinct at top-level
          // todo: check that arguments are constants
        case App(FunSym("distinct"), args) if
          topLevel && (getOptionStringValue(":expand-distinct") == Some("false")) =>
          "$distinct" + (args map { toTPTP(_, varSort, indent+1) }).mkString("(", ", ", ")")

        // Applications of associatove, chainable and pairwise operators:
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (leftAssocOps contains fun) =>
          toTPTP(expandLeftAssoc(fun, args), varSort, indent)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (rightAssocOps contains fun) =>
          toTPTP(expandRightAssoc(fun, args), varSort, indent)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (chainableOps contains fun) =>
          toTPTP(expandChainable(fun, args), varSort, indent)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (pairwiseOps contains fun) =>
          toTPTP(expandPairwise(fun, args), varSort, indent)

        // special cases
        case App(FunSym("="), List(s, t)) if (s.xsort(varSort) == BoolSort) =>
          // could be longish, print in two lines
          "( " + toTPTP(s, varSort, indent + 2) +
            nlspaces(indent - 2) + "<=> " + toTPTP(t, varSort, indent + 2) + ")"

        case App(FunSym("="), List(s, t)) =>
          "(" + toTPTP(s, varSort, indent + 1) + " = " + toTPTP(t, varSort, indent + 1) + ")"

        case App(FunSym("not"), List(App(FunSym("="), List(s, t)))) if s.xsort(varSort) != BoolSort =>
          "(" + toTPTP(s, varSort, indent + 1) + " != " + toTPTP(t, varSort, indent + 1) + ")"

        case App(FunSym("distinct"), List(s, t)) =>
          "(" + toTPTP(s, varSort, indent + 1) + " != " + toTPTP(t, varSort, indent + 1) + ")"

        case App(FunSym("and"), args) => args.length match {
          case 0 => "$true"
          case 1 => toTPTP(args(0), varSort, indent)
          case _ => toTPTPList(args, varSort, "(", "&", ")", indent)
        }
        case App(FunSym("or"), args) => args.length match {
          case 0 => "$false"
          case 1 => toTPTP(args(0), varSort, indent + 1)
          case _ => toTPTPList(args, varSort, "(", "|", ")", indent)
        }
        case App(FunSym("not"), List(arg)) => "(~ " + toTPTP(arg, varSort, indent + 3) + ")"

        case App(FunSym(op), List(s, t)) if op == "implies" || op == "=>" =>
          "( " + toTPTP(s, varSort, indent + 2) + nlspaces(indent - 1) + "=> " + toTPTP(t, varSort, indent + 2) + ")"

        case App(FunSym("ite"), List(c, t, e)) =>
          (if (t.xsort(varSort) == BoolSort) "$ite_f(" else "$ite_t(") +
            toTPTP(c, varSort, indent + 7) + "," +
            nlspaces(indent + 2) + toTPTP(t, varSort, indent + 2) + "," +
            nlspaces(indent + 2) + toTPTP(e, varSort, indent + 2) + ")"

        // "ordinary" function applications
        case App(fun @ FunSym(name), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          funRanks += ((fun, argsSorts, resSort))
          funToTPTP(fun, argsSorts, resSort) + (args map { toTPTP(_, varSort, indent + 1) }).toMyString("", "(", ", ", ")")
        }

        case App(As(fun @ FunSym(name), asSort), args) => {
          sorts += asSort
          val argsSorts = args map { _.xsort(varSort) }
          funRanks += ((fun, argsSorts, asSort))
          funToTPTP(fun, argsSorts, asSort) + (args map { toTPTP(_, varSort, indent + 1) }).toMyString("", "(", ", ", ")")
        }

        case Exists(varsWithSorts, body) => {
          "( ? " + (varsWithSorts map { varWithSortToTPTP(_) }).toMyString("[", ", ", "]") + " :" +
            nlspaces(indent + 4) + toTPTP(body, varSort ++ varsWithSorts.map({case (v,s) => (v,s.expanded)}), indent + 4) + ")"
        }

        case Forall(varsWithSorts, body) => {
          "( ! " + (varsWithSorts map { varWithSortToTPTP(_) }).toMyString("[", ", ", "]") + " :" +
            nlspaces(indent + 4) + toTPTP(body, varSort ++ varsWithSorts.map({case (v,s) => (v,s.expanded)}), indent + 4) + ")"
        }

        case Let(bindings, body) => {
          val bodyVarSort = varSort ++ (bindings map { case Equal(v, t) => (v, t.xsort(varSort)) })
          (if (body.xsort(bodyVarSort) == BoolSort) "$let_vf(" else "$let_vt(") +
            nlspaces(indent + 2) + (bindings map { case Equal(v, t) => varToTPTP(v) + "=" + toTPTP(t, varSort, indent + 4) }).toMyString("[", ", ", "]") + "," +
            nlspaces(indent + 2) + toTPTP(body, bodyVarSort, indent + 2) + ")"
        }

        case _ => throw this.Error("cannot convert formula: " + t)
      }
    }

    // body of print

    // Don't need to write declarations for predefined operators
    val tffPredefFuns = Set(FunSym("true"), FunSym("false"), FunSym("<"), FunSym(">"), FunSym("<="), FunSym(">="), FunSym("+"), FunSym("-"), FunSym("*"), FunSym("/"))

    // as a side effect, this will populate sorts and funRanks
    val tptpAssertions = assertions map { a =>
        assert(a._1.xsort() == BoolSort)
        // Negate an assertion if it is a conjecture
        val f = 
          if (isConjecture(a._1)) {
            // Complement
            a._1 match {
              case App(FunSym("not"), List(g)) => g
              case other => App(FunSym("not"), List(other))
            }
          } else a._1
        toTPTP(f, Map.empty, 2, topLevel = true)
      }

    out.println("%---Types:")
    for (
      sort <- sorts;
      if (!(tffPredefSorts contains sort))
    ) out.println("tff(" + toAtomicWord(sort.sortSym.name) + ", type, " + sortToTPTP(sort) + ": $tType).")
    out.println()

    out.println("%---Declarations:")
    for (
      funRank <- funRanks;
      if (!(tffPredefFuns contains funRank._1))
    ) out.println("tff(" + toAtomicWord(funRank._1.name) + ", type, " + funRankToTPTP(funRank) + ").")
    out.println()

    out.println("%---Assertions:")
    var formulaCtr = 1
    for ((a, f) <- assertions zip tptpAssertions) {
      out.println("%---" + a._2)
      if (defineAssertions(a._2))
        out.println("tff(formula_" + formulaCtr + ", definition," + nlspaces(2) + f + ").")
      else
        getStatusAttribute(a._1) match {
          case None => out.println("tff(formula_" + formulaCtr + ", axiom," + nlspaces(2) + f + ").")
          case Some((status, name)) => out.println("tff(" + name + ", " + status + "," + nlspaces(2) + f + ").")
        }
        // out.println("tff(formula_" + formulaCtr + ", axiom," + nlspaces(2) + f + ").")
      formulaCtr += 1
      out.println()
    }

  }

}
